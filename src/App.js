import { Route, Routes } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import ProtectedRoute from './components/ProtectedRoute';
import ResetPassDone from '../src/components/auth_component/ResetPassDone';
import ResetPassNext from '../src/components/auth_component/ResetPassNext';
import ResetPass from '../src/components/auth_component/ResetPass';
import Login from '../src/components/auth_component/Login';
import Register from '../src/components/auth_component/Register';
import Verifikasi from '../src/components/auth_component/Verif';
import LengkapiPendaftaran from '../src/components/auth_component/LoginNext';
import ResetPassVerif from './components/auth_component/ResetPassVerif';
import SearchResult from './components/home_component/SearchResult';
import ProductNotFound from './components/home_component/ProductNotFound';
import Discount from './components/home_component/Discount';
import Brand from './components/home_component/Brand';
import Category from './components/home_component/Category';
import Profile from './components/home_component/Profile';
import RiwayatPemesanan from './components/home_component/RiwayatPemesanan';
import DetailTransaksi from './components/home_component/DetailTransaksi';
import HomeTest from './pages/home_page/HomeTest';
import AuthHome from './pages/home_page/AuthHome';
import Auth from './pages/auth_page/Auth';
import DetailResult from './components/home_component/DetailResult';
import Checkout from './components/home_component/Checkout';
import Pembayaran from './components/home_component/Pembayaran';
import PembayaranBerhasil from './components/home_component/PembayaranBerhasil';
import Upload from './components/home_component/Upload';

function App() {
  const storage = window.localStorage;
  const token = storage.getItem('token');
  const decoded = token ? jwt_decode(token) : {};
  const isLoggedIn = decoded.exp > Date.now() / 1000;
  if (!isLoggedIn) {
  }
  return (
    <>
      <Routes>
        <Route element={<Auth />}>
          <Route path="login" element={<Login />}></Route>
          <Route path="register" element={<Register />}></Route>
          <Route path="resetpass" element={<ResetPass />}></Route>
          <Route path="resetpass_next" element={<ResetPassNext />}></Route>
          <Route path="resetpass_done" element={<ResetPassDone />}></Route>
          <Route path="resetpass_verif" element={<ResetPassVerif />}></Route>
          <Route path="verifikasi" element={<Verifikasi />}></Route>
          <Route path="lengkapi_pendaftaran" element={<LengkapiPendaftaran />}></Route>
        </Route>
        <Route path="/" element={<AuthHome />}>
          <Route path="/" element={<HomeTest />}></Route>
          <Route path="search_result" element={<SearchResult />}></Route>
          <Route path="search_result/:id" element={<DetailResult />}></Route>
          <Route path="product_not_found" element={<ProductNotFound />}></Route>
          <Route path="brand" element={<Brand />}></Route>
          <Route path="category" element={<Category />}></Route>
          <Route path="flashsale" element={<Discount />}></Route>

          <Route element={<ProtectedRoute />}>
            <Route path="profile" element={<Profile />} />

            <Route path="checkout" element={<Checkout />}></Route>
            <Route path="pembayaran/:id" element={<Pembayaran />}></Route>
            <Route path="pembayaran_berhasil" element={<PembayaranBerhasil />}></Route>
            <Route path="upload_pembayaran/:id" element={<Upload />}></Route>
            <Route path="riwayat_pemesanan" element={<RiwayatPemesanan />}></Route>
            <Route path="detail_transaksi/:id" element={<DetailTransaksi />}></Route>
          </Route>
        </Route>
      </Routes>
    </>
  );
}

export default App;
