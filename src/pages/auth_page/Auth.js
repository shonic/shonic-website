import { Outlet } from 'react-router-dom';
import FotAuth from '../../components/auth_component/FotAuth';
import NavAuth from '../../components/auth_component/NavAuth';

const Auth = () => {
  return (
    <>
      <NavAuth />
      <Outlet />;
      <FotAuth />
    </>
  );
};

export default Auth;
