import { Outlet } from 'react-router-dom';
import NavHome from '../../components/home_component/NavHome';
import FooterHome from '../../components/home_component/FooterHome';
import FotHome from '../../components/home_component/FotHome';

const AuthHome = () => {
  return (
    <>
      <NavHome />
      <Outlet />
      <FooterHome />
      <FotHome />
    </>
  );
};

export default AuthHome;
