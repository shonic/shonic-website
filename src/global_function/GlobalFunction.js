import { Star0, Star1, Star2, Star3, Star4, Star5 } from '../images/icons/Star';
export const bintang = (rating) => {
  switch (Math.floor(rating)) {
    case 0:
      return <Star1 className="star" />;
    case 1:
      return <Star1 className="star" />;
    case 2:
      return <Star2 className="star" />;
    case 3:
      return <Star3 className="star" />;
    case 4:
      return <Star4 className="star" />;
    case 5:
      return <Star5 className="star" />;
    default:
      return <Star0 className="star" />;
  }
};

export const formatRupiah = (money) => {
  return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }).format(money);
};
