import { PostImage } from '../../images/icons/Navigation';
import styles from './Upload.module.scss';
import { useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import { postUploadPembayaran } from '../action';
import { PembayaranCheck } from '../../images/icons/Penilaian';

const Upload = () => {
  const params = useParams();
  const history = useNavigate();
  const dispatch = useDispatch();
  const { paymentData } = useSelector((state) => state.checkout_cart_reducer);
  const [bank_name, setBankName] = useState('');
  const [bank_number, setBankNumber] = useState('');
  const [id, setId] = useState(`${params.id}`);
  const [name, setName] = useState('');
  const [photo, setPhoto] = useState('');
  const [total_transfer, setTotalTransfer] = useState(0);
  const storage = window.localStorage;
  const token = storage.getItem('token');

  return (
    <>
      <form className="container">
        <div className={styles.upload}>
          <div className={`${styles.upload_heading} bold-14-res`}>Bukti Pembayaran</div>
          <div className={styles.upload_box} type="file">
            <div className={styles.absolute}>
              {/* <PostImage /> */}
              {photo !== '' ? <PembayaranCheck /> : <PostImage />}
              {photo === '' ? <div className="medium-12-res">Unggah bukti pembayaran dengan format JPG, JPEG, atau PNG</div> : <div className="medium-12-res">Bukti pembayaran sudah terpilih</div>}
            </div>
            <input
              type="file"
              accept="image"
              className={styles.upload_boxa}
              id="img"
              placeholder="Unggah bukti pembayaran dengan format JPG, JPEG, atau PNG"
              onChange={(e) => {
                setPhoto(e.target.files[0]);
              }}
            />
          </div>
          <div className={`${styles.detail_heading} `}>
            <div className="bold-16-res">Detail Pembayaran</div>
            <div
              className={`${styles.reset} medium-12-res`}
              onClick={(e) => {
                setBankName('');
                setBankNumber('');
                setName('');
                setPhoto('');
                setTotalTransfer(0);
              }}>
              Reset
            </div>
          </div>
          <div className={styles.upload_detail}>
            <div className={`${styles.detail_main} medium-12-14`}>
              <label htmlFor="">Nama Bank</label>
              <input
                type="text"
                onChange={(e) => {
                  setBankName(e.target.value);
                }}
              />
            </div>
            <div className={`${styles.detail_main} medium-12-14`}>
              <label htmlFor="">Nama Pemilik Rekening</label>
              <input
                type="text"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
            </div>
            <div className={`${styles.detail_main} medium-12-14`}>
              <label htmlFor="">Nomor Rekening</label>
              <input
                type="number"
                onChange={(e) => {
                  setBankNumber(e.target.value);
                }}
              />
            </div>
            <div className={`${styles.detail_main} medium-12-14`}>
              <label htmlFor="">Jumlah Transfer</label>
              <input
                type="number"
                onChange={(e) => {
                  setTotalTransfer(e.target.value);
                }}
              />
              <p className="regular-12">Masukkan hanya angka tanpa titik maupun koma</p>
            </div>
          </div>
          <ul className={`${styles.detail_text} regular-10-res`}>
            <li>Isi form berdasarkan rekening yang Anda gunakan ketika melakukan transaksi</li>
            <li>Untuk pembayaran melalui teller, isi “Nama Pemilik Rekening” dengan nama Anda dan “No. Rekening” dengan 4444.</li>
          </ul>
          <button
            className={styles.send}
            onClick={(e) => {
              e.preventDefault();
              dispatch(postUploadPembayaran(bank_name, bank_number, id, name, photo, total_transfer, token, history));
            }}>
            Kirim
          </button>
        </div>
      </form>
    </>
  );
};

export default Upload;
