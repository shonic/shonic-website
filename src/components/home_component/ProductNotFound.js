import styles from './ProductNotFound.module.scss';
import no_product from '../../images/pictures/no_product.png';
import Card from './Card';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProduct } from '../action';
import RemahRoti from './RemahRoti';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

function handleClick(event) {
  event.preventDefault();
}

export default function ProductNotFound() {
  const dispatch = useDispatch();
  const { listSearch, list, error, loading } = useSelector((state) => state.homepage_reducer);
  const breadcrumbs = ['/'];

  useEffect(() => {
    dispatch(getAllProduct());
  }, [dispatch]);

  return (
    <>
      <div className={styles.search_container}>
        <RemahRoti />

        {listSearch !== null ? (
          <div className={`${styles.product_not_found_container} container`}>
            <div className={styles.no_product}>
              <div className={styles.imgku}>
                <img className={styles.imglost} src={no_product} alt="product not found" />
              </div>
              <div className={styles.no_product__title}>
                <div className={`${styles.card_price_discount} bold-16-20-28`}>Produk tidak ditemukan</div>
                <p className={`medium-12-res`}>Silakan coba kata kunci lain untuk menemukan produk yang Anda cari</p>
              </div>
            </div>

            <div className={styles.no_product_recommendation}>
              <h3 className={`${styles.header} bold-16-20-28`}>Mungkin Anda Suka</h3>
              {!loading && !error && (
                <section>
                  <Card products={listSearch} />
                </section>
              )}
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </>
  );
}
