import styles from './DetailTransaksi.module.scss';
import style from './PopUp.module.scss';
import { PenilaianBerhasil } from '../../images/icons/Penilaian';
import Popup from 'reactjs-popup';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import jwt_decode from 'jwt-decode';

const PopUpReviewBerhasil = ({ open, onClose }) => {
  const dispatch = useDispatch();
  const { fullName, username, password } = useSelector((state) => state.auth_reducer);
  const storage = window.localStorage;
  const token = storage.getItem('token');

  const [rating, setRating] = useState(0);
  const [review, setReview] = useState('');
  const postReviewProduct = (rating, review) => {};

  return (
    <Popup open={open} closeOnDocumentClick onClose={onClose}>
      <section className={styles.berhasil}>
        <div className={styles.berhasilbox}>
          <PenilaianBerhasil />
          <p className="bold-24">Penilaian Berhasil</p>
          <p className="regular-16">Terimakasih sudah memberikan penilaian</p>
        </div>
      </section>
    </Popup>
  );
};

export default PopUpReviewBerhasil;
