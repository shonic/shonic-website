import styles from './DetailTransaksi.module.scss';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import { fetchRefreshStatePesanan, getOrderDetail, postBatalkanPesanan, postReceived, refreshReview } from '../action';
import { formatRupiah } from '../../global_function/GlobalFunction';
import PopUpReview from './PopUpReview';
import PopUpReviewBerhasil from './PopUpReviewBerhasil';
import PopUpRiwayat from './PopUpRiwayat';

const DetailTransaksi = () => {
  const params = useParams();
  const history = useNavigate();
  const dispatch = useDispatch();

  const { orderDetail, reviewSuccess, statusDiterima, statusDibatalkan } = useSelector((state) => state.checkout_cart_reducer);

  const storage = window.localStorage;
  const token = storage.getItem('token');

  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);
  const [open2, setOpen2] = useState(true);
  const closeModal2 = () => setOpen2(false);

  const [tes, setTes] = useState(true);

  useEffect(() => {
    dispatch(getOrderDetail(params.id, token));
    dispatch(fetchRefreshStatePesanan);
  }, [reviewSuccess]);

  const batalkanPesanan = (id) => {
    dispatch(postBatalkanPesanan(id, token));
  };

  const pesananDiterima = (id) => {
    dispatch(postReceived(id, token));
  };

  return (
    <>
      <h1 className={`${styles.heading} bold-12-res-20-28`}>DETAIL TRANSAKSI</h1>
      {(Object.keys(orderDetail).length > 0) & (orderDetail !== null) ? (
        <div className="container">
          <div className={styles.card_wrapper}>
            <section className={`${styles.pembayaran}`}>
              <div className={styles.top}>
                <div className={styles.top1}>
                  <h2 className={`${styles} bold-12-res`}>No Pesanan</h2>
                  <p className="regular-12-res">{orderDetail['order_number']}</p>
                </div>
                <div className={styles.separator}></div>
                <div className={styles.top1}>
                  <h2 className={`${styles} bold-12-res`}>Tanggal Transaksi</h2>
                  <p className="regular-12-res">{orderDetail['date']}</p>
                </div>
                <div className={styles.separator}></div>
                <div className={styles.top1}>
                  <h2 className={`${styles} bold-12-res`}>Total Pembayaran</h2>
                  <p className="regular-12-res">{formatRupiah(orderDetail['total_price'])}</p>
                </div>
              </div>

              <div className={styles.bottom}>
                <div className={styles.waktu}>
                  <div className={styles.waktu_text}>
                    {orderDetail['status'] === 'Diproses' ? (
                      <p className="regular-12-res">{orderDetail['status_detail']}</p>
                    ) : orderDetail['status'] === 'Belum Bayar' ? (
                      <div className="BELUMBAYAR">
                        <p className="regular-12-res">Bayar pesanan sebelum</p>
                        <p className={`${styles.text_2} bold-12-res`}>{orderDetail['date']}</p>
                      </div>
                    ) : orderDetail['status'] === 'Dikirim' ? (
                      <div className="BELUMBAYAR">
                        <p className="regular-12-res">Estimasi pengiriman: </p>
                        <p className={`${styles.text_2} bold-12-res`}>{orderDetail['estimate']}</p>
                      </div>
                    ) : orderDetail['status'] === 'Selesai' && orderDetail['reviewed'] === false ? (
                      <div className="BELUMBAYAR">
                        <p className="regular-12-res">Nilai Pesanan Sebelum: </p>
                        <p className={`${styles.text_2} bold-12-res`}>{orderDetail['deadline']}</p>
                      </div>
                    ) : orderDetail['status'] === 'Selesai' && orderDetail['reviewed'] === true ? (
                      <div className="BELUMBAYAR">
                        <p className="regular-12-res">Pesanan telah dinilai </p>
                      </div>
                    ) : orderDetail['status'] === 'Dibatalkan' ? (
                      <div className="BELUMBAYAR">
                        <p className="regular-12-res">{orderDetail['status_detail']}</p>
                      </div>
                    ) : null}
                  </div>
                  <div className={styles.chip}>
                    {orderDetail['status'] === 'Belum Bayar' ? (
                      <div className={`${styles.chipbox} semibold-12-14`} style={{ border: '1px solid #D2C3EE', color: '#7940CC', backgroundColor: '#F4EFFF' }}>
                        {orderDetail['status']}
                      </div>
                    ) : orderDetail['status'] === 'Diproses' ? (
                      <div className={`${styles.chipbox} semibold-12-14`} style={{ border: '1px solid #EECEB0', color: '#734011', backgroundColor: '#FFF9F2' }}>
                        {orderDetail['status']}
                      </div>
                    ) : orderDetail['status'] === 'Dibatalkan' ? (
                      <div className={`${styles.chipbox} semibold-12-14`} style={{ border: '1px solid #EEB4B0', color: '#CB3A31', backgroundColor: '#FFF4F2' }}>
                        {orderDetail['status']}
                      </div>
                    ) : orderDetail['status'] === 'Selesai' ? (
                      <div className={`${styles.chipbox} semibold-12-14`} style={{ border: '1px solid #B8DBCA', color: '#20573D', backgroundColor: '#F5F5FF' }}>
                        {orderDetail['status']}
                      </div>
                    ) : orderDetail['status'] === 'Dikirim' ? (
                      <div className={`${styles.chipbox} semibold-12-14`} style={{ border: '1px solid #B1C5FF', color: '#3E7BFA', backgroundColor: '#ECF2FF' }}>
                        {orderDetail['status']}
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className={styles.tombol}>
                  {orderDetail['status'] === 'Belum Bayar' ? (
                    <div className={styles.tom}>
                      <Link to={`/pembayaran/${orderDetail['id']}`} className={styles.button}>
                        <button className={`${styles.button_blue} semibold-12-14`}>Bayar Sekarang</button>
                      </Link>
                      <button className={`${styles.button_white} semibold-12-14`} onClick={() => batalkanPesanan(orderDetail['id'])}>
                        Batalkan Pesanan
                      </button>
                    </div>
                  ) : orderDetail['status'] === 'Diproses' ? (
                    <div className={styles.tom}>
                      <button className={`${styles.button_proses} semibold-12-14`}>Pesanan Diterima</button>
                    </div>
                  ) : orderDetail['status'] === 'Dibatalkan' ? (
                    <div className={styles.tom}>
                      <Link to={`/search_result/${orderDetail['product']['id']}`} className={styles.button}>
                        <button className={`${styles.button_blue} semibold-12-14`}>Beli Lagi</button>
                      </Link>
                    </div>
                  ) : orderDetail['status'] === 'Dikirim' ? (
                    <div className={styles.tom}>
                      <button
                        className={`${styles.button_blue} semibold-12-14`}
                        onClick={(e) => {
                          pesananDiterima(orderDetail['id']);
                        }}>
                        Pesanan Diterima
                      </button>
                    </div>
                  ) : orderDetail['status'] === 'Selesai' && orderDetail['reviewed'] === false ? (
                    <div className={styles.tom}>
                      <button className={`${styles.button_blue} semibold-12-14`} onClick={() => setOpen((o) => !o)}>
                        Beri Penilaian
                      </button>
                      <Link to={`/search_result/${orderDetail['product']['id']}`} className={styles.button}>
                        <button className={`${styles.button_white} semibold-12-14`}>Beli Lagi</button>
                      </Link>
                    </div>
                  ) : orderDetail['status'] === 'Selesai' && orderDetail['reviewed'] === true ? (
                    <div className={styles.tom}>
                      <Link to={`/search_result/${orderDetail['product']['id']}`} className={styles.button}>
                        <button className={`${styles.button_blue} semibold-12-14`}>Beli Lagi</button>
                      </Link>
                    </div>
                  ) : null}
                </div>
              </div>
            </section>
            <section className={styles.pembelian}>
              <div className={styles.pembelianbox}>
                <div className="bold-20">Produk Yang Dibeli</div>
                <div className={styles.pem_top}>
                  <img className={styles.pem_img} src={orderDetail['product']['image_url']} alt="laptop" />
                  <div className={styles.pem_txt}>
                    <p className="bold-20-res">{orderDetail['product']['name']}</p>
                    <div className={styles.txt_flex}>
                      <p className="bold-20-res">{formatRupiah(orderDetail['product']['price_after_disc'])}</p>
                      {orderDetail['product']['discount'] > 0 ? (
                        <>
                          <p className="regular-12-res" style={{ textDecoration: 'line-through' }}>
                            {formatRupiah(orderDetail['product']['price'])}
                          </p>
                          <p className={styles.chipdisc}>{orderDetail['product']['discount']}%</p>
                        </>
                      ) : null}
                    </div>
                    <div className={styles.txt_flex2}>
                      <p className="regular-14-res">Kuantitas : {orderDetail['product']['qty']}</p>
                      <p className={`${styles.txt_blue} semibold-12-res`}>{formatRupiah(orderDetail['total_price'])}</p>
                    </div>
                  </div>
                </div>
                <div className={styles.pem_bot}>
                  <h3 className="semibold-12-14">Catatan</h3>
                  {orderDetail['note'].length > 0 ? <p className="regular-14">{orderDetail['note']}</p> : <p className="regular-14">Tidak ada</p>}
                </div>
              </div>
            </section>
            <section className={styles.rincian}>
              <div className={styles.flex1}>
                <div className={styles.xxxa}>
                  <p className={`${styles.head} bold-20`}>Alamat Pengiriman</p>
                  <p className="semibold-16-res">
                    {orderDetail['address']['name']}, {orderDetail['address']['phone']}
                  </p>
                  <p className="regular-14">
                    {orderDetail['address']['detail_address']}, {orderDetail['address']['city']['name']}, {orderDetail['address']['province']['name']}
                  </p>
                  <p className="regular-14">
                    {orderDetail['address']['kecamatan']}, {orderDetail['address']['province']['name']}, {orderDetail['address']['postalCode']}
                  </p>
                </div>
                <div className={styles.separator}></div>
                <div className={styles.xxxb}>
                  <p className={`${styles.head} bold-20`}>Jasa Pengiriman</p>
                  <p className="semibold-16-res">
                    {orderDetail['courier_code'].toUpperCase()}. Resi {orderDetail['resi']}
                  </p>
                </div>
              </div>
              <div className={styles.separator}></div>
              <div className={styles.flex2}>
                <div className={styles.xxxc}>
                  <p className={`${styles.head} bold-20`}>Rincian Belanja</p>
                  <div>
                    <div className={styles.flex}>
                      <p className="regular-14">Subtotal Produk</p>
                      <p className="semibold-16-res"> {formatRupiah(orderDetail['subtotal'])}</p>
                    </div>
                    <div className={styles.flex}>
                      <p className="regular-14">Subtotal Ongkos Kirim</p>
                      <p className="semibold-16-res"> {formatRupiah(orderDetail['cost'])}</p>
                    </div>
                    <div className={styles.flex}>
                      <p className="regular-14">Total Pembayaran</p>
                      <p className="semibold-16-res" style={{ color: '#3E7BFA' }}>
                        {formatRupiah(orderDetail['total_price'])}
                      </p>
                    </div>
                  </div>
                </div>
                <div className={styles.separator}></div>
                <div className={styles.xxxd}>
                  <p className={`${styles.head} bold-20`}>Metode Pembayaran</p>
                  <p className="semibold-16-res">{orderDetail['payment']}</p>
                </div>
              </div>
            </section>
            {reviewSuccess === true ? (
              <>
                <PopUpReviewBerhasil open={open2} onClose={closeModal2} />
              </>
            ) : null}
            <PopUpReview open={open} pesanan={orderDetail} id={orderDetail['id']} />
          </div>
        </div>
      ) : null}
      {statusDiterima === true ? <PopUpRiwayat open={open2} pesanan={'Diterima'} pesananText={'Pesanan berhasil diterima! Terima kasih telah berbelanja di Shonic!'} /> : null}
      {statusDibatalkan === true ? <PopUpRiwayat open={open2} pesanan={'Dibatalkan'} pesananText={'Pesanan berhasil dibatalkan!'} /> : null}
    </>
  );
};

export default DetailTransaksi;
