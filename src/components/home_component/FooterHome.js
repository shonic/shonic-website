import styles from './FooterHome.module.scss';

const FooterHome = () => {
  return (
    <>
      {/* FOOTER SECTION */}
      <section className={styles.footer_main}>
        <div className={`${styles.footer_grid} container`}>
          <div className={styles.footer_child1}>
            <h4 className="semibold-16-res">Tentang Shonic</h4>
            <p className="light-14">
              Temukan apapun kebutuhan peralatan komputer dan gadget kamu dengan harga terbaik cuma di Shonic. Shonic adalah pusat perbelanjaan online di mana kamu bisa mendapatkan update produk terkini dari kami dan dipastikan barang yang
              kami jual adalah original. Berbelanja menjadi lebih menyenangkan dengan fitur bagikan ke media sosial, di mana kamu bisa menyebarkan produk yang kamu jual atau pun barang favorit kamu hanya dengan satu klik saja. Belanja semua
              kebutuhanmu di Shonic dengan hati yang tenang! Cek rating dan review toko-toko yang ada dan temukan penjual yang terpercaya dengan mudah. Kami selalu mengutamakan keamanan transaksi untuk para pembeli kami!
            </p>
          </div>
          <div className={styles.footer_child2}>
            <div className={styles.kat}>
              <h5 className="semibold-16-res">Kategori</h5>
              <div className="light-14">
                <p>Audio</p>
                <p>Speaker</p>
                <p>Komponen</p>
                <p>Pc/Desktop</p>
                <p>Handphone</p>
                <p>Laptop</p>
              </div>
            </div>
            <div className={styles.faq}>
              <h5 className="semibold-16-res">Frequently Asked Question (F.A.Q)</h5>
            </div>
            <div className={styles.ikuti}>
              <h5 className="semibold-16-res">Ikuti Kami</h5>
              <div className="light-14">
                <p>Instagram</p>
                <p>Facebook</p>
                <p>Twitter</p>
                <p>Youtube</p>
              </div>
            </div>
            <div className={styles.bantuan}>
              <h5 className="semibold-16-res">Pusat Bantuan</h5>
              <p className="light-14">Buka 24 jam setiap hari 021-456-789 shonic@gmail.com</p>
            </div>
            <div className={styles.privasi}>
              <h5 className="semibold-16-res">Ketentuan Kebijakan & Privasi</h5>
            </div>
            <div className={styles.layanan}>
              <h5 className="semibold-16-res">Layanan Pengaduan</h5>
              <p className="light-14">Direktorat Jenderal Perlindungan Konsumen dan Tertib Niaga Kementerian Perdagangan RI</p>
            </div>
            <div className={styles.download}>
              <h5 className="semibold-16-res">Download Aplikasi Shonic</h5>
            </div>
            <div className={styles.icon1}>
              <img className={styles.googleplay} src="https://i.postimg.cc/jSTqcQqC/appstore.png" alt="appstore" />
            </div>
            <div className={styles.icon2}>
              <img className={styles.appstore} src="https://i.postimg.cc/jdPdykbG/googleplay.png" alt="googleplay" />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default FooterHome;
