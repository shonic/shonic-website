import { Wallet } from '../../images/pictures/Wallet';
import styles from './PembayaranBerhasil.module.scss';
import { Link } from 'react-router-dom';

const PembayaranBerhasil = () => {
  return (
    <div className={styles.outer}>
      <div className={`${styles.flexcontainer} container`}>
        <div className={styles.flexbottom}>
          <h3 className={`${styles.h3} bold-16-20-28`}>Pembayaran Selesai</h3>
          <Wallet />
          <p className={`${styles.syarat} medium-12`}>Silakan tunggu 1x24 jam untuk proses pengecekan pembayaran oleh Admin</p>
          <Link to="/riwayat_pemesanan">
            <button className={`${styles.button} semibold-16`}>Lihat Daftar Transaksi</button>
          </Link>
          <Link to="/">
            <button className={`${styles.back} semibold-16`}>kembali ke beranda</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default PembayaranBerhasil;
