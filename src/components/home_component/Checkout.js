import styles from './Checkout.module.scss';
import { Decrement, Increment, Truck } from '../../images/icons/Card';
import PopUp from '../home_component/PopUp';
import { formatRupiah } from '../../global_function/GlobalFunction';
import RemahRoti from './RemahRoti';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getShippingCostButton, getShippingCost, getMyAddress, decrementCart, deleteCart, incrementCart, getAllAccount, getAllCities, onCheckOut } from '../action';
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Alert } from '@mui/material';

const Checkout = () => {
  const history = useNavigate();
  const dispatch = useDispatch();
  const closeModal = () => setOpen(false);
  const { account, cart } = useSelector((state) => state.homepage_reducer);
  const { shipping, savedAddress } = useSelector((state) => state.checkout_cart_reducer);
  const storage = window.localStorage;
  const token = storage.getItem('token');
  const [code, setCode] = useState('');
  const [service, setService] = useState('');
  const [note, setNote] = useState('');
  const [payment_account_id, setPaymentAccountId] = useState(0);
  const [productId, setProductId] = useState('');
  const [qty, setQty] = useState(1);
  const [bank, setBank] = useState('');
  const [pembayaran, setPembayaran] = useState('Pilih Bank');
  const [etd, setEtd] = useState('X');
  const [jasaPengiriman, setJasaPengiriman] = useState('Pilih Jasa Pengiriman');
  const [ongkosKirim, setOngkosKirim] = useState(0);
  const [open, setOpen] = useState(false);
  let subTotalProduk = 0;

  useEffect(() => {
    dispatch(getAllCities());
    dispatch(getAllAccount());
    dispatch(getMyAddress(token));
    if (cart.length > 0) {
      dispatch(getShippingCost(cart[0], token));
    }
  }, []);
  if (shipping.length > 0) {
  }
  const onCheckout = () => {
    let total = subTotalProduk + ongkosKirim;
    dispatch(onCheckOut(total, history, token, code, service, note, payment_account_id, productId, qty));
  };
  const [buka, setBuka] = useState(false);

  let navigate = useNavigate();
  const handleClick = () => {
    setBuka(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setBuka(false);
  };

  const action = (
    <>
      <Button color="secondary" size="small" onClick={handleClose}></Button>
      <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </>
  );

  return (
    <>
      <RemahRoti />
      <Snackbar open={buka} autoHideDuration={2000} onClose={handleClose} message="Pesanan Dibatalkan" action={action}>
        <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
          Pemesanan Berhasil Dibatalkan
        </Alert>
      </Snackbar>

      <div className={styles.pesan}>
        <section className="container">
          {cart.map(({ id, image, name, price, discount, subHarga, subQuantity, after_discount }) => {
            if (id) {
              {
                subTotalProduk += subHarga;
              }

              return (
                <div className={styles.pesanan} key={id}>
                  <div className={styles.pesanan_1}>
                    <img className={styles.one_image} src={image} alt="laptop" />
                  </div>
                  <div className={styles.pesanan_2}>
                    <p className="semibold-12-res">{name}</p>
                  </div>
                  <div className={styles.pesanan_3}>
                    <div className={styles.three_flex}>
                      <p className="bold-12-res">{formatRupiah(price)}</p>
                      {discount !== 0 && (
                        <div className={styles.three_chip}>
                          {}
                          {discount}% OFF
                        </div>
                      )}
                    </div>
                  </div>
                  <div className={styles.pesanan_4}>
                    <div className={styles.four_topbox}>
                      <p className={`${styles.kuantitas} medium-12-res-2`}>Kuantitas</p>
                      <p className={`${styles.subtotal} bold-12-res`}>Subtotal</p>
                    </div>
                    <div className={styles.four_botbox}>
                      <div className={styles.four_flex_button}>
                        <button
                          onClick={() => {
                            dispatch(decrementCart(id));
                            dispatch(getShippingCostButton(id, subQuantity - 1, token));
                            setQty(subQuantity - 1);
                          }}
                          className={styles.four_increment}>
                          <Decrement className={styles.four_increment_svg} />
                        </button>
                        <div className="medium-12-res-2">{subQuantity}</div>
                        <button
                          onClick={() => {
                            dispatch(incrementCart(id));
                            dispatch(getShippingCostButton(id, subQuantity + 1, token));
                            setQty(subQuantity + 1);
                          }}
                          className={styles.four_increment}>
                          <Increment className={styles.four_increment_svg} />
                        </button>
                      </div>
                      <div className={`${styles.four_price} bold-12-res`}>{formatRupiah(subHarga)}</div>
                    </div>
                  </div>
                  <div className={styles.pesanan_5}>
                    <input
                      className={styles.five_input}
                      type="text"
                      placeholder="Catatan"
                      value={note}
                      onChange={(e) => {
                        setNote(e.target.value);
                      }}
                    />
                    <div className={styles.flex5}>
                      <p className="regular-12">dapat berupa variasi produk dan lainnya</p>
                      <div>
                        <button
                          className="medium-12-res-2"
                          onClick={() => {
                            handleClick();
                            dispatch(deleteCart(id));
                            navigate('/');
                          }}>
                          Batal Beli
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              );
            }
          })}
        </section>
        <section className="container">
          <div className={styles.co_bottom}>
            <div className={styles.co_1}>
              <div className={styles.one_header}>
                <div className={styles.alamat_pengiriman}>
                  <div className={styles.topside}>
                    <h3 className={`${styles} bold-16-res`}>Alamat Pengiriman</h3>
                    <p type="button" className={`${styles.ubah} medium-16`} onClick={() => setOpen((o) => !o)}>
                      Ubah Alamat
                    </p>
                  </div>
                  {savedAddress !== null ? (
                    Object.keys(savedAddress).length > 0 ? (
                      <div className={styles.downside}>
                        <p className={`${styles} semibold-16`}>
                          {savedAddress['name']} • {savedAddress['phone']}
                        </p>
                        <p className={`${styles} regular-16`}>
                          {savedAddress['detail']}
                          {savedAddress['detail_address']}, {savedAddress['kecamatan']}, {savedAddress['postalCode']}
                        </p>
                      </div>
                    ) : null
                  ) : null}
                </div>
                <PopUp open={open} onClose={closeModal} />
              </div>
              <div className=""></div>
            </div>
            <div className={styles.co_2}>
              <div className={styles.two_header}>
                <Truck />
                <p className="semibold-12-res">Jasa Pengiriman Dan Pembayaran</p>
              </div>
              <div className={styles.two_input_box}>
                <div className={styles.two_box}>
                  <label className="medium-12-res">Jasa Pengiriman</label>
                  <div className={styles.filter_dropdown}>
                    <button className={`${styles.dropdown} medium-14`}>
                      <div>{jasaPengiriman}</div>
                      <div className={styles.isi_dropdown}>
                        {shipping !== null
                          ? Object.keys(shipping).length > 0
                            ? shipping.map((shipping) => (
                                <option
                                  value={shipping['name']}
                                  onClick={(e) => {
                                    {
                                      setEtd(shipping['costs'][0]['cost'][0]['etd']);
                                    }
                                    setJasaPengiriman(`${shipping['name']} ${formatRupiah(shipping['costs'][0]['cost'][0]['value'])}`);
                                    setCode(shipping['code']);
                                    setService(shipping['costs'][0]['service']);
                                    setOngkosKirim(shipping['costs'][0]['cost'][0]['value']);
                                    setProductId(cart[0]['id']);
                                    //console.log('setService', service, 'code', code);
                                    // setService(shipping['costs'][0]['cost'][0]['value'])
                                  }}>
                                  {shipping['name']} {formatRupiah(shipping['costs'][0]['cost'][0]['value'])}
                                </option>
                              ))
                            : null
                          : null}
                      </div>
                    </button>
                    <p className={`${styles} regular-12`}>Estimasi pengiriman {etd} hari</p>
                  </div>
                </div>

                <div className={styles.two_box}>
                  <label className="medium-12-res">Pembayaran</label>
                  <div className={styles.filter_dropdown}>
                    <button className={`${styles.dropdown} medium-14`}>
                      <div>{pembayaran}</div>
                      <div className={styles.isi_dropdown}>
                        {account !== null
                          ? Object.keys(account).length > 0
                            ? account.map((bank) => (
                                <option
                                  value={account}
                                  onClick={(e) => {
                                    //console.log('bank', bank['id']);
                                    {
                                      setBank(bank['bank']);
                                    }
                                    setPembayaran(`Bank ${bank['bank']}`);
                                    setPaymentAccountId(bank['id']);
                                  }}>
                                  Bank {bank['bank']}
                                </option>
                              ))
                            : null
                          : null}
                      </div>
                    </button>
                    <p className={`${styles} regular-12`}>*Semua pembayaran melalui proses pengecekan manual oleh Admin.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.co_3}>
              <p className="medium-12-res">Detail Transaksi</p>
              <div className={styles.subtotal}>
                <p className="regular-12-res">Subtotal produk</p>
                <p className="bold-12-res">{formatRupiah(subTotalProduk)}</p>
              </div>
              <div className={styles.ongkir}>
                <p className="regular-12-res">Subtotal ongkos kirim</p>
                <p className="bold-12-res">{formatRupiah(ongkosKirim)}</p>
              </div>
              <div className={styles.garis}>a</div>
              <div className={styles.pembayaran}>
                <p className="bold-12-res">Total Pembayaran</p>
                <p className="bold-12-res">{formatRupiah(subTotalProduk + ongkosKirim)}</p>
              </div>
              <button
                className={styles.button3}
                onClick={() => {
                  dispatch(onCheckout);
                }}>
                checkout
              </button>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default Checkout;
