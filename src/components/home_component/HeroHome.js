import styles from './HeroHome.module.scss';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import { Layanan1, Layanan2, Layanan3, Layanan4 } from '../../images/icons/Layanan';

const HeroHome = () => {
  return (
    <>
      <div className={styles.border}>
        <div className={`${styles.content} container`}>
          <div className={styles.content_main}>
            <Carousel className={styles.content_carousel} showThumbs={false} dynamicHeight={true} showStatus={false} showIndicators={true} showArrows={false} infiniteLoop={true} autoPlay={true} interval={5000} stopOnHover={false}>
              <div className={styles.img_primary} style={{ backgroundImage: `url(https://i.postimg.cc/sfKwS6xy/carousel1.png)` }}></div>
              <div className={styles.img_primary} style={{ backgroundImage: `url(https://i.postimg.cc/prnr2c18/carousel2.png)` }}></div>
              <div className={styles.img_primary} style={{ backgroundImage: `url(https://i.postimg.cc/XNZkGVRK/carousel3.png)` }}></div>
              <div className={styles.img_primary} style={{ backgroundImage: `url(https://i.postimg.cc/Sx46vcJV/carousel4.png)` }}></div>
              <div className={styles.img_primary} style={{ backgroundImage: `url(https://i.postimg.cc/6QwvMKbP/carousel5.png)` }}></div>
            </Carousel>
          </div>
          <div className={styles.content_child1} style={{ backgroundImage: `url(https://i.postimg.cc/brPWJsmT/carousel1a.png)` }}></div>
          <div className={styles.content_child2} style={{ backgroundImage: `url(https://i.postimg.cc/tCX8GnZR/carousel1b.png)` }}></div>
        </div>
        <div className={`${styles.layanan_grid} container medium-12-res`}>
          <div className={styles.layanan1}>
            <Layanan1 />
            <div className={styles.layanan_flex}>
              <p className="medium-12-res">Garansi Ori 100%</p>
              <p className="regular-12-res">menjamin produk original</p>
            </div>
          </div>
          <div className={styles.layanan2}>
            <Layanan2 />
            <div className={styles.layanan_flex}>
              <p className="medium-12-res">Pengiriman se-Indonesia</p>
              <p className="regular-12-res">Menjangkau wilayah terpencil</p>
            </div>
          </div>
          <div className={styles.layanan3}>
            <Layanan3 />
            <div className={styles.layanan_flex}>
              <p className="medium-12-res">Kemudahan akses</p>
              <p className="regular-12-res">Tersedia versi mobile</p>
            </div>
          </div>
          <div className={styles.layanan4}>
            <Layanan4 />
            <div className={styles.layanan_flex}>
              <p className="medium-12-res">Penawaran harian</p>
              <p className="regular-12-res">Diskon mencapai 70%</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeroHome;
