//css //
import styles from './PopUp.module.scss';
// icons //
import { RiArrowDropDownLine } from 'react-icons/ri';
// actions //
import { saveAddress, getAllProvinces, getAllCitiesByProvince, getAllCities } from '../action';
// necessary dependencies //
import Popup from 'reactjs-popup';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import jwt_decode from 'jwt-decode';

const PopUp = ({ open, onClose }) => {
  // const dispatch = useDispatch();
  // const { account } = useSelector((state) => state.homepage_reducer);
  // useEffect(() => {
  //   // refresh verifAcc & emailStatus state value
  //   dispatch(getAllAccount())
  // }, []);
  // console.log("accounts popup", account['bank'])
  const dispatch = useDispatch();
  const { savedAddress, allProvinces, allCitiesByProvince, allCities } = useSelector((state) => state.checkout_cart_reducer);

  const [chosenProvince, setChosenProvince] = useState('Pilih Provinsi');
  const [chosenCity, setChosenCity] = useState('Pilih Kota/Kabupaten');

  const [city_id, setCityId] = useState(0);
  const [detail, setDetail] = useState('');
  const [kecamatan, setKecamatan] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState(0);
  const [postal_code, setPostalCode] = useState('');
  const [province_id, setProvinceId] = useState(0);

  const { fullName, username, password } = useSelector((state) => state.auth_reducer);
  const storage = window.localStorage;
  const token = storage.getItem('token');
 // console.log("savedAddress", savedAddress)

  useEffect(() => {
    dispatch(getAllProvinces());
    dispatch(getAllCities());
    // refresh verifAcc & emailStatus state value
  }, []);

  return (
    <Popup open={open} closeOnDocumentClick onClose={onClose}>
      <div className={styles.modal}>
        <div className={styles.modal_content}>
          <form>
            <div className={styles.informasi_penerima}>
              <h3 className={`${styles.h3} bold-20`}>Informasi Penerima</h3>
              <div className={styles.box}>
                <div className={styles.isi_box}>
                  <label for="fname" className={`${styles} medium-14`}>
                    Nama Penerima
                  </label>
                  <input
                    type="text"
                    id="fname"
                    name="firstname"
                    placeholder="Contoh: Rachelin Anastasia"
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                      //console.log("kecamatan", kecamatan)
                    }}></input>
                </div>

                <div className={styles.isi_box}>
                  <label for="fname" className={`${styles} medium-14`}>
                    Nomor Handphone
                  </label>
                  <input
                    type="number"
                    id="fname"
                    name="firstname"
                    placeholder="Contoh: 08927289182"
                    value={phone}
                    onChange={(e) => {
                      setPhone(e.target.value);
                      //console.log("kecamatan", kecamatan)
                    }}></input>
                </div>
              </div>
            </div>
            <div className={styles.alamat_penerima}>
              <h3 className={`${styles} bold-20`}>Alamat Penerima</h3>
              <div className={styles.box}>
                <div className={styles.isi_box}>
                  <label for="fname" className={`${styles} medium-14`}>
                    Provinsi{' '}
                  </label>
                  <div className={styles.provinsi_dropdown}>
                    <button className={`${styles.dropdown} medium-14`} onClick={(e) => e.preventDefault()}>
                      {chosenProvince} <RiArrowDropDownLine style={{ position: 'relative', top: '2px' }} />
                      <div className={styles.isi_dropdown} onClick={(e) => e.preventDefault()}>
                        {allProvinces.map((provinsi) => (
                          <option
                            value={provinsi['name']}
                            onClick={(e) => {
                              e.preventDefault();
                              setChosenProvince(provinsi['name']);
                              setProvinceId(provinsi['id']);
                              dispatch(getAllCitiesByProvince(provinsi['id']));
                            }}>
                            {provinsi['name']}
                          </option>
                        ))}
                      </div>
                    </button>
                  </div>
                </div>
                <div className={styles.isi_box}>
                  <label for="fname" className={`${styles} medium-14`} onClick={(e) => e.preventDefault()}>
                    Kota/Kabupaten
                  </label>
                  <div className={styles.provinsi_dropdown}>
                    <button className={`${styles.dropdown} medium-14`} onClick={(e) => e.preventDefault()}>
                      {chosenCity}
                      <RiArrowDropDownLine style={{ position: 'relative', top: '2px' }} />
                      <div className={styles.isi_dropdown}>
                        {allCitiesByProvince.length > 0 ? (
                          allCitiesByProvince.map((kota) => (
                            <option
                              value={kota['name']}
                              onClick={(e) => {
                                e.preventDefault();
                                setChosenCity(kota['name']);
                                setCityId(kota['id']);
                              }}>
                              {kota['name']}
                            </option>
                          ))
                        ) : (
                          <>
                            {allCities.map((kota) => (
                              <option
                                value={kota['name']}
                                onClick={(e) => {
                                  e.preventDefault();
                                  setChosenCity(kota['name']);
                                  setCityId(kota['id']);
                                }}>
                                {kota['name']}
                              </option>
                            ))}
                          </>
                        )}
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className={styles.box}>
                <div className={styles.isi_box}>
                  <label for="fname" className={`${styles} medium-14`}>
                    Kecamatan
                  </label>
                  <input
                    type="text"
                    id="fname"
                    name="firstname"
                    placeholder="Contoh: Bandung Barat"
                    value={kecamatan}
                    onChange={(e) => {
                      setKecamatan(e.target.value);
                      //console.log("kecamatan", kecamatan)
                    }}></input>
                </div>
                <div className={styles.isi_box}>
                  <label for="fname" className={`${styles} medium-14`}>
                    Kode Pos
                  </label>
                  <input
                    type="number"
                    id="fname"
                    name="firstname"
                    placeholder="Contoh: 20028"
                    value={postal_code}
                    onChange={(e) => {
                      setPostalCode(e.target.value);
                      //console.log("kecamatan", kecamatan)
                    }}></input>
                </div>
              </div>

              <div className={styles.alamat}>
                <div className={styles.isi_alamat}>
                  <label for="fname" className={`${styles} medium-14`}>
                    Alamat
                  </label>
                  <textarea
                    id="story"
                    name="story"
                    rows="3"
                    cols="33"
                    placeholder="  Contoh: Jalan Melodi IV No 15"
                    value={detail}
                    onChange={(e) => {
                      setDetail(e.target.value);
                      //console.log("kecamatan", kecamatan)
                    }}></textarea>
                </div>
              </div>
            </div>
          </form>

          <div className={styles.buttons}>
            <button
              className={`${styles.simpan} semibold-16`}
              onClick={(e) => {
                onClose();
                dispatch(saveAddress(city_id, detail, kecamatan, name, phone, postal_code, province_id, token));
              }}>
              Simpan
            </button>
            <button className={`${styles.kembali} semibold-16`} onClick={onClose}>
              Kembali
            </button>
          </div>
        </div>
      </div>
    </Popup>
  );
};

export default PopUp;
