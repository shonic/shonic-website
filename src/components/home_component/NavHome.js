// styling //
import styles from './NavHome.module.scss';
import { Cart, Hamburger, RightClick, Search, Phone } from '../../images/icons/Navigation';
import { RiArrowDropDownLine } from 'react-icons/ri';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import { ShonicIcon, ShonicText } from '../../images/icons/ShonicIcon';
import { fetchRefreshStatePesanan, searchProductApi, getAllProduct, searchByCategoryTes, toggleNav, logOut, getProfile } from '../action';

const NavHome = () => {
  const dispatch = useDispatch();
  const history = useNavigate();
  const [productName, getProductName] = useState('');
  const { profile } = useSelector((state) => state.auth_reducer);

  const navLink = [
    {
      id: 1,
      name: 'Laptop',
    },
    {
      id: 2,
      name: 'PC/Desktop',
    },
    {
      id: 3,
      name: 'HandPhone',
    },
    {
      id: 4,
      name: 'Audio/Speaker',
    },
    {
      id: 5,
      name: 'Komponen',
    },
    {
      id: 6,
      name: 'Aksesoris',
    },
  ];

  const { hamburger } = useSelector((state) => state.nav_reducer);
  const storage = window.localStorage;
  const token = storage.getItem('token');
  const decoded = token ? jwt_decode(token) : {};
  const isLoggedIn = decoded.exp > Date.now() / 1000;

  useEffect(() => {
    dispatch(getAllProduct());
    dispatch(getProfile(token));
    dispatch(fetchRefreshStatePesanan);
  }, []);
  const searchProduct = (e) => {
    e.preventDefault();
    dispatch(searchProductApi(productName, history));
  };

  return (
    <>
      <div className={styles.topbar_wrap}>
        <div className={`${styles.topbar} medium-10-res container`}>
          <div className={styles.topbar_left}>
            <Phone />
            <div>Download via aplikasi</div>
          </div>
          <div className={styles.topbar_right}>
            <div>Bantuan</div>
            <div>FAQ</div>
            <div>Tentang Shonic</div>
          </div>
        </div>
      </div>

      <header className={styles.header}>
        <nav className={styles.nav}>
          <div className={`${styles.nav_wrapper} container`}>
            <Link to="/">
              <div className={styles.nav_icon}>
                <ShonicIcon className={styles.logo} />
                <ShonicText className={styles.text} />
              </div>
            </Link>
            <button
              onClick={() => {
                dispatch(toggleNav());
              }}
              className={styles.nav_hamburger}>
              <Hamburger />
            </button>
            <form className={styles.nav_search}>
              <input className={styles.nav_search_input} value={productName} onChange={(e) => getProductName(e.target.value)} type="text" placeholder="Cari produk" />
              <button className={styles.nav_search_icon} onClick={(e) => searchProduct(e)}>
                <Search />
              </button>
            </form>
            <div className={styles.nav_carts}>
              <button className={styles.nav_carts_cart}>
                <Link to={'/riwayat_pemesanan'}>
                  <Cart />
                </Link>
              </button>
              {isLoggedIn === true && profile !== undefined ? (
                <>
                  <Link to={'/profile'}>
                    <p className={`${styles.username} medium-14`}>{profile['nama']}</p>
                  </Link>
                  <div className={styles.filter_dropdown}>
                    <button className={`${styles.dropdown} medium-14`}>
                      <RiArrowDropDownLine size={25} className="username_arrow" />
                      <div className={styles.isi_dropdown}>
                        <Link to={'/profile'}>
                          <option>Akun Saya</option>
                        </Link>
                        <Link to={'/riwayat_pemesanan'}>
                          <option>Daftar Pesanan</option>
                        </Link>
                        <option
                          onClick={(e) => {
                            dispatch(logOut(token));
                            localStorage.removeItem('token');
                            window.location.reload();
                          }}>
                          Keluar
                        </option>
                      </div>
                    </button>
                  </div>
                </>
              ) : (
                <Link to="/login" className={`${styles.nav_login} medium-14`}>
                  Masuk
                </Link>
              )}
            </div>
          </div>
          <div className={`${styles.nav_bottom_hidden} container ${hamburger === true ? styles.open : styles.close}`}>
            {isLoggedIn === true ? (
              <div className={styles.username_profile}></div>
            ) : (
              <Link to="/login" className={`${styles.nav_login} medium-14`}>
                <button className={styles.nav_bottom_hidden_login}>Masuk</button>
              </Link>
            )}
            <div className={styles.nav_bottom_hidden_kategori}>
              <div className={`${styles.nav_bottom_hidden_mainlist} bold-14`}>Kategori</div>
              {navLink.map(({ id, name, brand }) => (
                <>
                  <details
                    onClick={() => {
                      dispatch(searchByCategoryTes(history, `${name}`));
                      dispatch(toggleNav());
                    }}
                    open={hamburger === true ? false : true}
                    className={`${styles.nav_bottom_hidden_list} regular-12`}
                    key={id}>
                    <summary className={styles.summary}>
                      <div>{name}</div>
                      {/* <RightClick /> */}
                    </summary>
                  </details>
                </>
              ))}
              {isLoggedIn === true && profile !== undefined ? (
                <div className={styles.user_navbar}>
                  <Link
                    to={'/profile'}
                    onClick={() => {
                      dispatch(toggleNav());
                    }}>
                    <p className={`${styles.akun} bold-14`}>Akun Saya</p>
                  </Link>
                  <Link
                    to={'/riwayat_pemesanan'}
                    onClick={() => {
                      dispatch(toggleNav());
                    }}>
                    <p className={`${styles.akun} bold-14`}>Pesanan Saya</p>
                  </Link>
                  <p
                    className={`${styles.akun} bold-14`}
                    onClick={(e) => {
                      dispatch(logOut(token));
                      localStorage.removeItem('token');
                      window.location.reload();
                      dispatch(toggleNav());
                    }}>
                    Keluar
                  </p>
                </div>
              ) : null}
            </div>
          </div>
        </nav>
        <div
          onClick={() => {
            dispatch(toggleNav());
          }}
          className={`${styles.backgroundblur} ${hamburger === false ? styles.openbg : styles.closebg}`}></div>
      </header>
      <div className={styles.desktop_bottom}>
        <div className={styles.desktop_flex}>
          {navLink.map(({ id, name, brand }) => (
            <div key={id} className={styles.desktop_flex_content}>
              <details className={styles.desktop_details}>
                <summary
                  className={`${styles.desktop_summary} regular-14`}
                  onClick={() => {
                    dispatch(searchByCategoryTes(history, `${name}`));
                  }}>
                  <div className={styles.namakategori}>{name}</div>
                </summary>
              </details>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default NavHome;
