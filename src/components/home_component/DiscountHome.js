import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styles from './DiscountHome.module.scss';
import { bintang, formatRupiah } from '../../global_function/GlobalFunction';
import { Like } from '../../images/icons/Card';
import { getAllProduct, getFlashSale } from '../action';

const DiscountHome = () => {
  const dispatch = useDispatch();
  const { listFlash } = useSelector((state) => state.homepage_reducer);

  useEffect(() => {
    dispatch(getAllProduct());
    dispatch(getFlashSale());
  }, []);

  const splitJam = (x) => {
    return x.split(':')[0];
  };
  const splitMenit = (x) => {
    return x.split(':')[1];
  };
  const splitDetik = (x) => {
    return x.split(':')[2];
  };

  return (
    <>
      {listFlash !== null && listFlash !== undefined && Object.keys(listFlash).length > 0 ? (
        <section className={`${styles.disc} container`}>
          <div className={`${styles.disc_header} bold-14-res`}>DISCOUNT SPECIAL</div>
          {listFlash !== null && listFlash !== undefined && Object.keys(listFlash).length > 0 ? (
            <div className={styles.topflex}>
              <div className={styles.no_product_recommendation_left}>
                <p className={`${styles.p} medium-12-res`}>Berakhir Dalam</p>
                <div className={styles.buttons}>
                  <button className={styles.button}>{splitJam(listFlash.countdown)}</button> :<button className={styles.button}>{splitMenit(listFlash.countdown)} </button>:
                  <button className={styles.button}> {splitDetik(listFlash.countdown)}</button>
                </div>
              </div>
              <Link className={`${styles.lihatsemua} medium-12-res`} to={'/flashsale'}>
                Lihat Semua
              </Link>
            </div>
          ) : (
            <div className=""> </div>
          )}
          <div className={styles.disc_flex_main}>
            <div className={styles.fix}>
              <img className={styles.imgfix} src="https://i.postimg.cc/vTHCk6Sh/discount.jpg" alt="diskon" />
            </div>
            <div className={`${styles.product_container}`}>
              <div className="swiffy-slider slider-item-show3 slider-nav-round slider-nav-page slider-help">
                <ul className="slider-container" style={{ gridAutoColumns: 'unset' }}>
                  {listFlash !== null && listFlash !== undefined && Object.keys(listFlash).length > 0 ? (
                    listFlash.products.slice(0, 10).map(({ id, image, discount, name, price, rating, after_discount }) => {
                      if (discount !== null) {
                        return (
                          <Link to={'/search_result/' + id} style={{ width: '100%', height: '100%', textDecoration: 'none', color: '#1c1c1e' }}>
                            <li key={id}>
                              <div className={styles.disc_container}>
                                <div className={styles.disc_top}>
                                  <div className={styles.disc_photo} style={{ backgroundImage: `url(${image})` }}></div>
                                </div>
                                <div className={styles.disc_bottom}>
                                  <p className={`${styles.disc_name} medium-12-res`}>{name}</p>
                                  <div className={`${styles.disc_price_main} bold-14-res-disc`}>{formatRupiah(after_discount)}</div>
                                  <div className={styles.disc_prices}>
                                    <div className={`${styles.disc_price_discount} medium-10-res`}>{formatRupiah(price)}</div>
                                    {parseInt(discount) > 0 ? <div className={styles.disc_chip}>{discount}%</div> : null}
                                  </div>
                                  <div className={styles.disc_rate}>
                                    <div className={styles.disc_star}>
                                      <div>{bintang(rating)}</div>
                                      <p className={`${styles.disc_star_rate} regular-10-res`}>{rating}</p>
                                    </div>
                                    <div className={styles.disc_love}>
                                      <Like />
                                    </div>
                                  </div>
                                  <button className={styles.disc_button}>LIHAT DETAIL</button>
                                </div>
                              </div>
                            </li>
                          </Link>
                        );
                      } else {
                        return null;
                      }
                    })
                  ) : (
                    <div></div>
                  )}
                </ul>
                <button type="button" className="slider-nav" aria-label="Go left"></button>
                <button type="button" className="slider-nav slider-nav-next" aria-label="Go left"></button>
              </div>
            </div>
          </div>
        </section>
      ) : (
        <div className=""> </div>
      )}
    </>
  );
};

export default DiscountHome;
