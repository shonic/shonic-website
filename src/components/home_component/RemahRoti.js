import { Breadcrumbs as MUIBreadcrumbs, Link } from '@mui/material';
import { useNavigate, useLocation } from 'react-router-dom';
import { RightArrowBread } from '../../images/icons/Navigation';

import { useSelector } from 'react-redux';

const RemahRoti = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const { brand } = useSelector((state) => state.homepage_reducer);

  const pathnames = pathname.split('/').filter(Boolean);
  const isiNama = (x) => {
    switch (x) {
      case 'search_result':
        return 'Hasil Pencarian';
      case 'profile':
        return 'Akun Saya';
      case 'category':
        return 'Kategori';
      case 'riwayat_pemesanan':
        return 'Riwayat Pemesanan';
      case 'subcategory':
        return 'Sub Kategori';
      case 'product':
        return 'Produk';
      case 'product_detail':
        return 'Detail Produk';
      case 'cart':
        return 'Cart';
      case 'flashsale':
        return 'Flash Sale';
      case 'checkout':
        return 'Halaman Pembelian';
      default:
        return x;
    }
  };
  return (
    <div className="breadmargin">
      <div className="bread container">
        <MUIBreadcrumbs aria-label="breadcrumb" separator={<RightArrowBread />}>
          {pathnames.length ? (
            <Link className="regular-12-res breadcrumb" underline="none" color="#505050" onClick={() => navigate('/')}>
              Home
            </Link>
          ) : (
            <p className="regular-12-res breadcrumb">Home</p>
          )}
          {pathnames.map((name, index) => {
            const routeTo = `/${pathnames.slice(0, index + 1).join('/')}`;
            const isLast = index === pathnames.length - 1;
            let upperBrand = brand.toUpperCase();
            return isLast ? (
              <p className="regular-12-res breadcrumb" color="#505050" key={name}>
                {/* {brand ? <p>{name === 'search_result' ? 'Hasil Pencarian' : name}</p> : <p>{name === 'search_result' ? 'Hasil Pencarian' : name}</p>} */}
                {/* {brand ? <p>{name === 'search_result' ? 'Hasil Pencarian' : name}</p> : <p>{isiNama(name)}</p>} */}
                {isiNama(name)}

                {/* make as a switch statement */}
              </p>
            ) : (
              <Link className="regular-12-res breadcrumb" color="#505050" key={name} onClick={() => navigate(routeTo)}>
                {name === 'search_result' ? 'Hasil Pencarian' : name}
              </Link>
            );
          })}
        </MUIBreadcrumbs>
      </div>
    </div>
  );
};

export default RemahRoti;
