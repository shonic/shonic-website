import styles from './DetailTransaksi.module.scss';
import style from './PopUp.module.scss';
import StarsRating from 'react-star-rate';
import { postReviewProduct } from '../action';
import Popup from 'reactjs-popup';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import jwt_decode from 'jwt-decode';
import PopUpReviewBerhasil from './PopUpReviewBerhasil';

const PopUpReview = ({ open, onClose, pesanan, id }) => {
  const dispatch = useDispatch();
  const { reviewSuccess } = useSelector((state) => state.checkout_cart_reducer);
  const storage = window.localStorage;
  const token = storage.getItem('token');

  const [rating, setRating] = useState(0);
  const [review, setReview] = useState('');

  const [open2, setOpen] = useState(false);
  const closeModal2 = () => setOpen(false);

  // console.log("open", open, "onClose", onClose)
  // console.log("open", open, "onClose", onClose)

  // const reviewProduct = (rating, review) => {
  //     // console.log("ini postReviewProduct", rating, review)
  //     dispatch(postReviewProduct(rating, review, id, token))
  // }

  return (
    <Popup open={open} closeOnDocumentClick onClose={onClose}>
      <section className={styles.ulasan}>
        <div className={`${styles.ulasanbox} `}>
          <div className={`${styles.header} bold-20`}>Rating dan Ulasan</div>
          <div className={styles.produk}>
            <img className={styles.image} src={pesanan['product']['image_url']} alt="thumbnail" />
            <div className={`${styles.name} semibold-14`}>{pesanan['product']['name']}</div>
          </div>
          <div className={styles.star}>
            <div className={`${styles.rating} medium-14`}>Rating :</div>
            <StarsRating
              value={rating}
              onChange={(rating) => {
                setRating(rating);
              }}
            />
          </div>
          <div className={styles.inputbox}>
            <div className={`${styles.inputheader} medium-14`}>Ulasan :</div>
            <textarea className={styles.input} value={review} onChange={(e) => setReview(e.target.value)} placeholder="Tulis ulasanmu disini..." type="text" />
          </div>
          <div className={styles.boxbot_container}>
            <div className={styles.boxbot}>
              <button
                className={styles.but_blue}
                onClick={() => {
                  dispatch(postReviewProduct(rating, review, id, token));
                  //open = false;
                  setOpen((o) => !o);
                  //onClose()
                }}>
                Kirim
              </button>
              <PopUpReviewBerhasil open={open2} onClose2={closeModal2} />
              {/* {open2 === true ? 
                                onClose() : 
                            console.log("false")} */}
              <button className={styles.but_white} onClick={onClose}>
                Kembali
              </button>
            </div>
          </div>
        </div>
      </section>
    </Popup>
  );
};

export default PopUpReview;
