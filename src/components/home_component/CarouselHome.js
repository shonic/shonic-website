import styles from './CarouselHome.module.scss';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Star } from '../../images/icons/Card';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getAllProduct } from '../action';
import HeroHome from './HeroHome';
import CategoryHome from './CategoryHome';
import DiscountHome from './DiscountHome';
import BrandPilihanHome from './BrandPilihanHome';
import { bintang, formatRupiah } from '../../global_function/GlobalFunction';
import Card from '../home_component/Card';

const CarouselHome = () => {
  const [rekomendasi, setRekomendasi] = useState('5');
  const [isReadMore, setIsReadMore] = useState(true);
  const dispatch = useDispatch();
  const { list, error, loading, rekomendasiState, rekomendasiData } = useSelector((state) => state.homepage_reducer);
  let number = 0;
  let number2 = 0;
  const products2 = [];
  useEffect(() => {
    dispatch(getAllProduct());
  }, [dispatch]);
  const conditionalCss = {
    rekomendasiToggle0: {
      fontWeight: rekomendasi === '5' ? '700' : '400',
      borderBottom: rekomendasi === '5' ? '2px solid #3a42ff' : '1px solid #9e9e9e',
    },
    rekomendasiToggle1: {
      fontWeight: rekomendasi === 'handphone' ? '700' : '400',
      borderBottom: rekomendasi === 'handphone' ? '2px solid #3a42ff' : '1px solid #9e9e9e',
    },
    rekomendasiToggle2: {
      fontWeight: rekomendasi === 'komponen komputer' ? '700' : '400',
      borderBottom: rekomendasi === 'komponen komputer' ? '2px solid #3a42ff' : '1px solid #9e9e9e',
    },
    rekomendasiToggle3: {
      fontWeight: rekomendasi === 'laptop' ? '700' : '400',
      borderBottom: rekomendasi === 'laptop' ? '2px solid #3a42ff' : '1px solid #9e9e9e',
    },
    rekomendasiToggle4: {
      fontWeight: rekomendasi === 'pc/desktop' ? '700' : '400',
      borderBottom: rekomendasi === 'pc/desktop' ? '2px solid #3a42ff' : '1px solid #9e9e9e',
    },
    rekomendasiToggle5: {
      fontWeight: rekomendasi === 'aksesoris' ? '700' : '400',
      borderBottom: rekomendasi === 'aksesoris' ? '2px solid #3a42ff' : '1px solid #9e9e9e',
    },
  };

  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };

  return (
    <>
      <HeroHome />
      <CategoryHome />
      <DiscountHome />
      <BrandPilihanHome />
      <section className={`${styles.produk} container`}>
        <h3 className={`${styles.produk_header} bold-14-res`}>REKOMENDASI PRODUK</h3>
        <div className={`${styles.produk_search} medium-12-res container`}>
          <p
            className={styles.produk_search_text}
            style={conditionalCss.rekomendasiToggle0}
            onClick={() => {
              setRekomendasi('5');
            }}>
            Semua
          </p>
          <p
            id="1"
            style={conditionalCss.rekomendasiToggle3}
            className={styles.produk_search_text}
            onClick={() => {
              setRekomendasi('laptop');
            }}>
            Laptop
          </p>
          <p
            style={conditionalCss.rekomendasiToggle4}
            className={styles.produk_search_text}
            onClick={() => {
              setRekomendasi('pc/desktop');
            }}>
            PC/Desktop
          </p>
          <p
            style={conditionalCss.rekomendasiToggle1}
            className={styles.produk_search_text}
            onClick={() => {
              setRekomendasi('handphone');
            }}>
            Handphone
          </p>
          <p
            style={conditionalCss.rekomendasiToggle5}
            className={styles.produk_search_text}
            onClick={() => {
              setRekomendasi('aksesoris');
            }}>
            Aksesoris
          </p>
          <p
            style={conditionalCss.rekomendasiToggle2}
            className={styles.produk_search_text}
            onClick={() => {
              setRekomendasi('komponen komputer');
            }}>
            Komponen
          </p>
        </div>
        <section className={`${styles.cards} container`}>
          {list !== null ? (
            list.map(({ id, name, index, category, discount, rating, review, image, price, after_discount }) => {
              number2++;
              if (rekomendasi === '4') {
                <p key={id}>barang tidak ada :c</p>;
              } else if (rekomendasi === '5' && isReadMore && number < 12) {
                number++;
                return (
                  <Link to={'/search_result/' + id} style={{ width: '100%', height: '100%', color: '#1c1c1e' }}>
                    <div className={styles.card_container} key={id}>
                      <div className={styles.card_top}>
                        <div className={styles.card_photo} style={{ backgroundImage: `url(${image})` }}></div>
                        {parseInt(discount) > 0 ? <div className={styles.card_chip}>{discount} % OFF</div> : null}
                      </div>
                      <div className={styles.card_bottom}>
                        <div className={`${styles.card_name} regular-10-res`}>{name}</div>
                        <div className={styles.card_prices}>
                          <div className={`${styles.card_price_main} semibold-12-res`}>{formatRupiah(after_discount)}</div>
                          {parseInt(discount) > 0 ? <div className={`${styles.card_price_discount} medium-10-res`}>{formatRupiah(price)}</div> : null}
                        </div>
                        <div className={styles.card_rate}>
                          <div className={styles.card_star}>
                            <Star className={styles.star} />
                            <div>{bintang(rating)}</div>
                            <p className={`${styles.card_star_rate} regular-10-res`}>
                              {rating} • {review} Ulasan
                            </p>
                          </div>
                        </div>
                        <button className={styles.card_button}>LIHAT DETAIL</button>
                      </div>
                    </div>
                  </Link>
                );
              } else if (rekomendasi === '5' && isReadMore === false) {
                return (
                  <Link to={'/search_result/' + id} style={{ width: '100%', height: '100%', color: '#1c1c1e' }}>
                    <div className={styles.card_container}>
                      <div className={styles.card_top}>
                        <div className={styles.card_photo} style={{ backgroundImage: `url(${image})` }}></div>
                        {parseInt(discount) > 0 ? <div className={styles.card_chip}>{discount}% OFF</div> : null}
                      </div>
                      <div className={styles.card_bottom}>
                        <div className={`${styles.card_name} regular-10-res`}>{name}</div>
                        <div className={styles.card_prices}>
                          <div className={`${styles.card_price_main} semibold-12-res`}>{formatRupiah(after_discount)}</div>
                          {parseInt(discount) > 0 ? <div className={`${styles.card_price_discount} medium-10-res`}>{formatRupiah(price)}</div> : null}
                        </div>
                        <div className={styles.card_rate}>
                          <div className={styles.card_star}>
                            <Star className={styles.star} />
                            <div>{bintang(rating)}</div>
                            <p className={`${styles.card_star_rate} regular-10-res`}>
                              {rating} • {review} Ulasan
                            </p>
                          </div>
                        </div>
                        <button className={styles.card_button}>LIHAT DETAIL</button>
                      </div>
                    </div>
                  </Link>
                );
              } else if (category.category_parent.name === rekomendasi && isReadMore && number < 10) {
                number++;
                return (
                  <Link to={'/search_result/' + id} style={{ width: '100%', height: '100%', color: '#1c1c1e' }}>
                    <div className={styles.card_container}>
                      <div className={styles.card_top}>
                        <div className={styles.card_photo} style={{ backgroundImage: `url(${image})` }}></div>
                        {parseInt(discount) > 0 ? <div className={styles.card_chip}>{discount}% OFF</div> : null}
                      </div>
                      <div className={styles.card_bottom}>
                        <div className={`${styles.card_name} regular-10-res`}>{name}</div>
                        <div className={styles.card_prices}>
                          <div className={`${styles.card_price_main} semibold-12-res`}>{formatRupiah(after_discount)}</div>
                          {parseInt(discount) > 0 ? <div className={`${styles.card_price_discount} medium-10-res`}>{formatRupiah(price)}</div> : null}
                        </div>
                        <div className={styles.card_rate}>
                          <div className={styles.card_star}>
                            <Star className={styles.star} />
                            <div>{bintang(rating)}</div>
                            <p className={`${styles.card_star_rate} regular-10-res`}>
                              {rating} • {review} Ulasan
                            </p>
                          </div>
                        </div>
                        <button className={styles.card_button}>LIHAT DETAIL</button>
                      </div>
                    </div>
                  </Link>
                );
              } else if (category.category_parent.name === rekomendasi && isReadMore === false) {
                return (
                  <Link to={'/search_result/' + id} style={{ width: '100%', height: '100%', color: '#1c1c1e' }}>
                    <div className={styles.card_container}>
                      <div className={styles.card_top}>
                        <div className={styles.card_photo} style={{ backgroundImage: `url(${image})` }}></div>
                        {parseInt(discount) > 0 ? <div className={styles.card_chip}>{discount}% OFF</div> : null}
                      </div>
                      <div className={styles.card_bottom}>
                        <div className={`${styles.card_name} regular-10-res`}>{name}</div>
                        <div className={styles.card_prices}>
                          <div className={`${styles.card_price_main} semibold-12-res`}>{formatRupiah(after_discount)}</div>
                          {parseInt(discount) > 0 ? <div className={`${styles.card_price_discount} medium-10-res`}>{formatRupiah(price)}</div> : null}
                        </div>
                        <div className={styles.card_rate}>
                          <div className={styles.card_star}>
                            <Star className={styles.star} />
                            <div>{bintang(rating)}</div>
                            <p className={`${styles.card_star_rate} regular-10-res`}>
                              {rating} • {review} Ulasan
                            </p>
                          </div>
                        </div>
                        <button className={styles.card_button}>LIHAT DETAIL</button>
                      </div>
                    </div>
                  </Link>
                );
              } else {
                return null;
              }
            })
          ) : (
            <p></p>
          )}
        </section>
        {list !== null ? (
          <button onClick={toggleReadMore} className={`${styles.read_or_hide} semibold-16`}>
            {isReadMore === false ? 'Show Less' : 'Show More'}
          </button>
        ) : null}
      </section>
    </>
  );
};

export default CarouselHome;
