import { Clock } from '../../images/pictures/Clock';
import styles from './Pembayaran.module.scss';
import { MdOutlineContentCopy } from 'react-icons/md';
import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
// actions //
import { loginActionAsync, fetchRefreshState, getPaymentData } from '../action';
// components //
import { formatRupiah } from '../../global_function/GlobalFunction';

const Pembayaran = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const { account, cartSubtotal } = useSelector((state) => state.homepage_reducer);
  const { paymentData } = useSelector((state) => state.checkout_cart_reducer);

  const storage = window.localStorage;
  const token = storage.getItem('token');

  useEffect(() => {
    dispatch(getPaymentData(params.id, token));
  }, []);

  return (
    <div className={styles.outer}>
      <div className={`${styles.flexcontainer} container`}>
        <div className={styles.flexup}>
          <Clock className={styles.pic} />
          <h3 className={`${styles.h3} bold-20`}>Batas Akhir Pembayaran</h3>
          <p className={`${styles.tanggal} semibold-16`}>{paymentData['pay_before']}</p>
          <button className={styles.button_bukti}>
            <Link to={`/upload_pembayaran/${paymentData['order_id']}`} className={`${styles.link} semibold-16`}>
              <div className={styles.button}>
                <p>Kirim Bukti Pembayaran</p>
              </div>
            </Link>
          </button>
          <div className={`${styles.accent} semibold-16`}>
            <div className={styles.strip}></div>
            <p className={`${styles.p} regular-14`}>atau</p>
            <div className={styles.strip}></div>
          </div>
          {/* GOOGLE LOGIN */}
          <button className={styles.buttongoogle}>
            <Link to="/riwayat_pemesanan" className={`${styles.link} semibold-16`}>
              <div className={styles.flex}>
                <p>Lihat Daftar Transaksi</p>
              </div>
            </Link>
          </button>
        </div>
        <div className={styles.flexright}>
          {paymentData !== null && Object.keys(paymentData).length > 0 ? (
            <div className={styles.flexbottom_container}>
              <div className={styles.bca}>
                <div className={styles.nama_bank}>
                  <img className={styles.one_image} src={paymentData['image']} alt="bank" />
                </div>
                <div className={styles.pt}>
                  <p className={`${styles.shonic} semibold-16`}>PT. Shonic Indonesia</p>
                  <p className={`${styles} regular-12`}>(Cabang {paymentData['branch']})</p>
                </div>
              </div>
              <div className={styles.rekening}>
                <div className={styles.no_rekening}>
                  <p className={`${styles.no} medium-14`}>No. Rekening:</p>
                  <p className={`${styles} bold-20`}>{paymentData['number']}</p>
                </div>
                <div className={`${styles.copas} regular-12`}>
                  <MdOutlineContentCopy
                    onClick={() => {
                      navigator.clipboard.writeText(paymentData['number']);
                    }}
                    style={{ color: '#1C1C1E', fontSize: '22px' }}
                  />
                </div>
              </div>
              <div className={styles.rekening}>
                <div className={styles.no_rekening}>
                  <p className={`${styles.no} medium-14`}>Total Pembayaran:</p>
                  <p className={`${styles.total} bold-20`}>{formatRupiah(paymentData['total_payment'])}</p>
                </div>
                <div className={`${styles.copas} regular-12`}>
                  <MdOutlineContentCopy
                    onClick={() => {
                      navigator.clipboard.writeText(paymentData['total_payment']);
                    }}
                    style={{ color: '#1C1C1E', fontSize: '22px' }}
                  />
                </div>
              </div>
              <p className={`${styles} regular-14`}>Bayar pesanan sesuai jumlah diatas sebelum batas akhir pembayaran untuk menghindari pembatalan otomatis.</p>
              <div className={`${styles.accent} semibold-16`}>
                <div className={styles.strip}></div>
              </div>
              <div className={styles.informasi_pembayaran}>
                <h3 className={`${styles} bold-20`}>Informasi Pembayaran</h3>
                <ul>
                  <li className={`${styles} regular-14`}>Gunakan ATM / iBanking / mBanking / setor tunai melakukan pembayaran</li>
                  <li className={`${styles} regular-14`}>Demi kenyamanan transaksi, mohon tidak membagikan bukti atau konfirmasi pembayaran pesanan kepada siapapun, selain mengunggahnya via website ataupun aplikasi Shonic</li>
                  <li className={`${styles} regular-14`}>Silahkan lengkapi form pembayaran dengan cara mengunggah bukti transfer yang telah Anda lakukan</li>
                </ul>
              </div>
            </div>
          ) : (
            <div className={styles.flexbottom_container}>
              <div className={styles.bca}>
                <div className={styles.nama_bank}>
                  <img className={styles.one_image} alt="laptop" />
                </div>
                <div className={styles.pt}>
                  <p className={`${styles.shonic} semibold-16`}>PT. Shonic Indonesia</p>
                  <p className={`${styles} regular-12`}>(Cabang)</p>
                </div>
              </div>
              <div className={styles.rekening}>
                <div className={styles.no_rekening}>
                  <p className={`${styles.no} medium-14`}>No. Rekening:</p>
                  <p className={`${styles} bold-20`}>8xxxx7xxxx</p>
                </div>
                <p className={`${styles.copas} regular-12`}>
                  <MdOutlineContentCopy style={{ color: '#1C1C1E', fontSize: '22px' }} />
                </p>
              </div>
              <div className={styles.rekening}>
                <div className={styles.no_rekening}>
                  <p className={`${styles.no} medium-14`}>Total Pembayaran:</p>
                  <p className={`${styles.total} bold-20`}></p>
                </div>
                <p className={`${styles.copas} regular-12`}>
                  <MdOutlineContentCopy style={{ color: '#1C1C1E', fontSize: '22px' }} />
                </p>
              </div>
              <p className={`${styles} regular-14`}>Bayar pesanan sesuai jumlah diatas sebelum batas akhir pembayaran untuk menghindari pembatalan otomatis.</p>
              <div className={`${styles.accent} semibold-16`}>
                <div className={styles.strip}></div>
              </div>
              <div className={styles.informasi_pembayaran}>
                <h3 className={`${styles} bold-20`}>Informasi Pembayaran</h3>
                <ul>
                  <li className={`${styles} regular-14`}>Gunakan ATM / iBanking / mBanking / setor tunai melakukan pembayaran</li>
                  <li className={`${styles} regular-14`}>
                    Demi kenyamanan transaksi, mohon tidak membagikan bukti atau konfirmasi pembayaran pesanan kepada siapapun, selain mengunggahnya via website ataupun aplikasi <a href="#">Shonic</a>
                  </li>
                  <li className={`${styles} regular-14`}>Silahkan lengkapi form pembayaran dengan cara mengunggah bukti transfer yang telah Anda lakukan</li>
                </ul>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Pembayaran;
