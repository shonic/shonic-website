// styling //
import styles from './DetailResult.module.scss';
import gambar from '../../images/pictures/laptop.jpg';
import { Check, Decrement, Increment, Like, Star } from '../../images/icons/Card';
import { Rating } from 'react-simple-star-rating';
import StarsRating from 'react-star-rate';
// necessary dependencies //
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
// actions //
import { getProductReview, getFamiliar, listSearch, incrementItem, getProductById, decrementItem, addToCart, addProductToCart } from '../action';
// components //
import RemahRoti from './RemahRoti';
import RemahRoti2 from './RemahRoti2';
import { bintang } from '../../global_function/GlobalFunction';
import DetailProps from './DetailProps';
import Card from './Card';
import { formatRupiah } from '../../global_function/GlobalFunction';

import { Link, useNavigate } from 'react-router-dom';
// import { useState } from 'react';
const DetailResult = () => {
  const params = useParams();
  const dispatch = useDispatch();
  let history = useNavigate();
  useEffect(() => {
    // console.log(`params id-------------> ${params.id}`);
    dispatch(getProductById(params.id));
    dispatch(getProductReview(params.id));
    dispatch(getFamiliar(params.id));
  }, [dispatch, params.id]);

  const { list, listId, listSearch, error, loading, listReview, listFamiliar } = useSelector((state) => state.homepage_reducer);
  const { detailQuantity, detailTotal } = useSelector((state) => state.detail_reducer);

  const [status, setStatus] = useState('Deskripsi');
  const [nilai, setNilai] = useState('Semua');
  const [active, setActive] = useState(true);

  // console.log("listFamiliar", listFamiliar)
  const conditionalCss = {
    nilaiToggleSemua: {
      background: nilai === 'Semua' ? '#3e7bfa' : '#ffffff',
      color: nilai === 'Semua' ? '#ffffff' : '#1c1c1e',
    },
    nilaiToggle5: {
      background: nilai === 5 ? '#3e7bfa' : '#ffffff',
      color: nilai === 5 ? '#ffffff' : '#1c1c1e',
    },
    nilaiToggle4: {
      background: nilai === 4 ? '#3e7bfa' : '#ffffff',
      color: nilai === 4 ? '#ffffff' : '#1c1c1e',
    },
    nilaiToggle3: {
      background: nilai === 3 ? '#3e7bfa' : '#ffffff',
      color: nilai === 3 ? '#ffffff' : '#1c1c1e',
    },
    nilaiToggle2: {
      background: nilai === 2 ? '#3e7bfa' : '#ffffff',
      color: nilai === 2 ? '#ffffff' : '#1c1c1e',
    },
    nilaiToggle1: {
      background: nilai === 1 ? '#3e7bfa' : '#ffffff',
      color: nilai === 1 ? '#ffffff' : '#1c1c1e',
    },
  };

  const conditionalCss2 = {
    deskripsi: {
      // background: status === 'Deskripsi' ? '#3e7bfa' : '#ffffff',
      // color: status === 'Deskripsi' ? '#ffffff' : '#1c1c1e',
      borderBottom: status === 'Deskripsi' ? '3px solid #3e7bfa' : 'none',
      marginTop: status === 'Deskripsi' ? '-2px' : '0px',
    },
    rating: {
      borderBottom: status === 'Rating dan Ulasan' ? '3px solid #3e7bfa' : 'none',
      marginTop: status === 'Rating dan Ulasan' ? '-2px' : '0px',
    },
  };

  // console.log("status", status);
  // console.log('listId', '==========================================================', listId);
  return (
    <>
      <RemahRoti2 />
      {Object.keys(listId).length !== 0 ? (
        <>
          {/* <button onClick={() => dispatch(addToCart())}>ADD TO CART </button> */}
          {/* SECTION 1 --- DISPlAYING PRODUCT ``````````````````````````````````````````````````````````````````````````````````*/}

          <div className={`${styles.detail_page} container`}>
            <div className={styles.detail_img} style={{ backgroundImage: `url(${listId.image})` }}></div>
            <div className={styles.detail_desc}>
              {/* TOP--------------------------------------- */}
              <div className={styles.detail_desc_top}>
                <div className={styles.desc_top_flex}>
                  <div className={`${styles.desc_top_1} font-detail`}>{listId.name}</div>
                  <div className={styles.desc_top_2}>
                    {listId.discount !== 0 ? <div className={`${styles.harga} font-detail-price`}>{formatRupiah(listId.after_discount)}</div> : <div className={`${styles.harga} font-detail-price`}>{formatRupiah(listId.price)}</div>}
                    {listId.discount !== 0 && <div className={`${styles.disc} medium-12-res`}>{formatRupiah(listId.price)}</div>}
                    {listId.discount !== 0 && <div className={`${styles.discchip} font-detail-chip`}>{listId.discount}% OFF</div>}
                  </div>
                  <div className={styles.desc_top_3}>
                    <div className={`${styles.terjual} regular-12-res`}>terjual 18</div>
                    <Star className={styles.star1} />
                    <div className={styles.star5}>{bintang(listId.rating)}</div>
                    <div className={`${styles.ulasan} regular-12-res`}>| {listId.review} ulasan</div>
                  </div>
                </div>
                {/* MID --------------------------------------- */}
                <div className={styles.detail_desc_mid}>
                  <Check className={styles.check} />
                  <div>
                    <p className="font-original">Produk Original 100%</p>
                    <p className="regular-10-res">Garansi produk resmi official store</p>
                  </div>
                </div>
                {/* BOT--------------------------------------- */}
                <div className={styles.detail_desc_bot}>
                  <p className="semibold-16-res">Rincian Produk</p>
                  <div className={`${styles.detail_desc_flex} regular-12-res`}>
                    <div className={styles.detail_rincian}>
                      <p className={styles.detail_desc_text}>Stok</p>
                      <p className={styles.detail_desc_text}>Merk</p>
                      <p className={styles.detail_desc_text}> Kategori</p>
                      {/* <p className={styles.detail_desc_text}>Berat</p> */}
                    </div>
                    <div className={`${styles.detail_detail} regular-12-res`}>
                      <p className={styles.detail_desc_text}>{listId.qty}</p>
                      <p className={styles.detail_desc_text}>{listId.brand.name}</p>
                      <p className={styles.detail_desc_text}>
                        {listId.category.category_parent.name} - {listId.category.name}
                      </p>
                      {/* <p className={styles.detail_desc_text}>2000gram</p> */}
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className={styles.detail_desc_mid}></div>
          <div className={styles.detail_desc_end}></div> */}
            </div>
          </div>
          {/* SECTION 2 --- ADD TO CART``````````````````````````````````````````````````````````````````````````````````*/}
          <section className={styles.addtocart_wrapper}>
            <div className={`${styles.addtocart_sticky} container`}>
              <div className={`${styles.addtocart_x} semibold-16`}>{listId.name}</div>
              <div className={styles.addtocart_flex}>
                {/* --------------------INCREMENT DECREMENT BUTTON HERE ---------------- */}
                {listId.discount !== 0 ? (
                  <>
                    <button onClick={() => dispatch(decrementItem(listId.after_discount))} className={styles.addtocart_increment}>
                      <Decrement className={styles.addtocart_increment_svg} />
                    </button>
                    <div className="semibold-12-res">{detailQuantity}</div>
                    <button onClick={() => dispatch(incrementItem(listId.after_discount))} className={styles.addtocart_increment}>
                      <Increment className={styles.addtocart_increment_svg} />
                    </button>
                  </>
                ) : (
                  <>
                    <button onClick={() => dispatch(decrementItem(listId.price))} className={styles.addtocart_increment}>
                      <Decrement className={styles.addtocart_increment_svg} />
                    </button>
                    {/* Ga Diskon */}
                    <div className="semibold-12-res">{detailQuantity} </div>
                    <button onClick={() => dispatch(incrementItem(listId.price))} className={styles.addtocart_increment}>
                      <Increment className={styles.addtocart_increment_svg} />
                    </button>
                  </>
                )}
              </div>
              <div className={styles.addtocart_price}>
                <p className="medium-12-res">Total</p>
                <p className="bold-16-res">{formatRupiah(detailTotal)}</p>
              </div>
              {/* <button className={styles.addtocart_y}>
                <Like className={styles.y_like} />
                <div className={styles.wishlist}>Add To Wishlist</div>
              </button> */}
              {/*   ##################################################33333333333333333333333333333333333333 */}
              {/* <button onClick={() => dispatch(addProductToCart(listId.id, detailTotal, detailQuantity, history))} className={styles.addtocart_button}>
                Add To Cart
              </button>  */}
              {listId.discount !== 0 ? (
                // <button onClick={() => dispatch(addProductToCart(listId.id, detailTotal, detailQuantity, listId.after_discount, history))} className={styles.addtocart_button}>
                //   Add To Cart DISKON
                // </button>
                <button onClick={() => dispatch(addProductToCart(listId.id, detailTotal, detailQuantity, listId.price, history))} className={styles.addtocart_button}>
                  Beli Sekarang
                </button>
              ) : (
                <button onClick={() => dispatch(addProductToCart(listId.id, detailTotal, detailQuantity, listId.price, history))} className={styles.addtocart_button}>
                  Beli Sekarang
                </button>
              )}
            </div>
          </section>
          {/* SECTION 3 --- DESKRIPSI PRODUK``````````````````````````````````````````````````````````````````````````````````*/}
          <div className="container">
            <div className={styles.deskripsi}>
              <div className={`${styles.deskripsi_flex} semibold-16-res`}>
                <div className={styles.title} style={conditionalCss2.deskripsi} onClick={(e) => setStatus('Deskripsi')}>
                  <h3
                    className={`${status.deskripsi} semibold-16-res`}
                    onClick={() => {
                      setStatus('Deskripsi');
                      // console.log("status", status)
                    }}>
                    Deskripsi Produk
                  </h3>
                  {/* <button style={conditionalCss2.deskripsi} className={`${styles.x} medium-14`} onClick={(e) => setNilai('Semua')}>
                        Deskripsi
                      </button> */}
                </div>
                <div className={styles.title} style={conditionalCss2.rating} onClick={(e) => setStatus('Rating dan Ulasan')}>
                  <h3
                    className={`${conditionalCss2.rating} semibold-16-res`}
                    onClick={() => {
                      setStatus('Rating dan Ulasan');
                      // console.log("status", status)
                    }}>
                    Rating dan Ulasan
                  </h3>
                  {/* <button style={conditionalCss2.rating} className={`${styles.x} medium-14`} onClick={(e) => setNilai('Semua')}>
                        Rating dan Ulasan
                      </button> */}
                </div>
              </div>
              {status === 'Deskripsi' ? (
                <pre className={`${styles.deskripsi_text} regular-12-res`}>{listId.description}</pre>
              ) : (
                <>
                  <div className={styles.rating_dan_ulasan}>
                    {/* <div>{bintang(4)}</div> */}
                    {/* <p className={`${styles.disc_star_rate} regular-10-res`}>4</p> */}
                    {/* <Rating ratingValue={4}/> */}
                    <div className={styles.bintang}>
                      <StarsRating classNamePrefix="ratingku" value={listReview['total_rating']} />
                      <h1 className={`${styles.angka} semibold-16-res`}>
                        {listReview['total_rating']}
                        <span className="semibold-12-res"> /5</span>
                      </h1>
                    </div>
                    <div className={styles.buttons}>
                      <button style={conditionalCss.nilaiToggleSemua} className={`${styles.x} medium-14`} onClick={(e) => setNilai('Semua')}>
                        Semua
                      </button>
                      <button
                        style={conditionalCss.nilaiToggle5}
                        className={`${styles.x} medium-14`}
                        onClick={(e) => {
                          setNilai(5);
                          // console.log('ya');
                        }}>
                        Rating 5
                      </button>
                      <button style={conditionalCss.nilaiToggle4} className={`${styles.x} medium-14`} onClick={(e) => setNilai(4)}>
                        Rating 4
                      </button>
                      <button style={conditionalCss.nilaiToggle3} className={`${styles.x} medium-14`} onClick={(e) => setNilai(3)}>
                        Rating 3
                      </button>
                      <button style={conditionalCss.nilaiToggle2} className={`${styles.x} medium-14`} onClick={(e) => setNilai(2)}>
                        Rating 2
                      </button>
                      <button style={conditionalCss.nilaiToggle1} className={`${styles.x} medium-14`} onClick={(e) => setNilai(1)}>
                        Rating 1
                      </button>
                    </div>
                    <div className={styles.reviews}>
                      {listReview['reviews'].map((list, index) => {
                        if (nilai === 'Semua') {
                          return (
                            <div className={styles.review}>
                              <div className={styles.review_info}>
                                <div className={styles.user_info}>
                                  <p className={`${styles} bold-14`}>{list['user_name']}</p>
                                  <p className={`${styles} regular-14`}>{list['date']}</p>
                                </div>
                                <div className={styles.user_bintang}>{bintang(list['rating'])}</div>
                              </div>
                              <p className={`${styles} regular-14`}>{list['review']}</p>
                            </div>
                          );
                        } else if (nilai === 5 && list['rating'] >= 5 && list['rating'] < 6) {
                          return (
                            <div className={styles.review}>
                              <div className={styles.review_info}>
                                <div className={styles.user_info}>
                                  <p className={`${styles} bold-14`}>{list['user_name']}</p>
                                  <p className={`${styles} regular-14`}>{list['date']}</p>
                                </div>
                                <div className={styles.user_bintang}>{bintang(list['rating'])}</div>
                              </div>
                              <p className={`${styles} regular-14`}>{list['review']}</p>
                            </div>
                          );
                        } else if (nilai === 4 && list['rating'] >= 4 && list['rating'] < 5) {
                          return (
                            <div className={styles.review}>
                              <div className={styles.review_info}>
                                <div className={styles.user_info}>
                                  <p className={`${styles} bold-14`}>{list['user_name']}</p>
                                  <p className={`${styles} regular-14`}>{list['date']}</p>
                                </div>
                                <div className={styles.user_bintang}>{bintang(list['rating'])}</div>
                              </div>
                              <p className={`${styles} regular-14`}>{list['review']}</p>
                            </div>
                          );
                        } else if (nilai === 3 && list['rating'] >= 3 && list['rating'] < 4) {
                          return (
                            <div className={styles.review}>
                              <div className={styles.review_info}>
                                <div className={styles.user_info}>
                                  <p className={`${styles} bold-14`}>{list['user_name']}</p>
                                  <p className={`${styles} regular-14`}>{list['date']}</p>
                                </div>
                                <div className={styles.user_bintang}>{bintang(list['rating'])}</div>
                              </div>
                              <p className={`${styles} regular-14`}>{list['review']}</p>
                            </div>
                          );
                        } else if (nilai === 2 && list['rating'] >= 2 && list['rating'] < 3) {
                          return (
                            <div className={styles.review}>
                              <div className={styles.review_info}>
                                <div className={styles.user_info}>
                                  <p className={`${styles} bold-14`}>{list['user_name']}</p>
                                  <p className={`${styles} regular-14`}>{list['date']}</p>
                                </div>
                                <div className={styles.user_bintang}>{bintang(list['rating'])}</div>
                              </div>
                              <p className={`${styles} regular-14`}>{list['review']}</p>
                            </div>
                          );
                        } else if (nilai === 1 && list['rating'] >= 1 && list['rating'] < 2) {
                          return (
                            <div className={styles.review}>
                              <div className={styles.review_info}>
                                <div className={styles.user_info}>
                                  <p className={`${styles} bold-14`}>{list['user_name']}</p>
                                  <p className={`${styles} regular-14`}>{list['date']}</p>
                                </div>
                                <div className={styles.user_bintang}>{bintang(list['rating'])}</div>
                              </div>
                              <p className={`${styles} regular-14`}>{list['review']}</p>
                            </div>
                          );
                        }
                      })}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
          {/* SECTION 4 --- MUNGKIN ANDA SUKA CARD``````````````````````````````````````````````````````````````````````````````````*/}
        </>
      ) : (
        <div className=""></div>
      )}
      {listFamiliar !== null ? (
        <div className={`${styles.product_not_found_container}`}>
          <div className={styles.no_product_recommendation}>
            <h3 className={`${styles.header} bold-14-res container`}>Mungkin Anda Suka</h3>
            {!loading && !error && (
              <section className={`container`}>
                <Card products={listFamiliar} />
              </section>
            )}
          </div>
        </div>
      ) : null}
    </>
  );
};

export default DetailResult;
