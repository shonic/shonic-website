//css //
import styles from './DetailTransaksi.module.scss';
import style from './PopUp.module.scss';
import laptop from '../../images/pictures/laptop.jpg';
// icons // 
import { RiArrowDropDownLine } from "react-icons/ri";
import StarsRating from 'react-star-rate';
import { PenilaianBerhasil } from '../../images/icons/Penilaian';
// actions //
import { saveAddress, getAllProvinces, getAllCitiesByProvince, getAllCities } from '../action';
// necessary dependencies //
import Popup from 'reactjs-popup'
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import jwt_decode from 'jwt-decode'

const PopUpReviewBerhasil = ({open,  onClose, pesanan, pesananText}) => {
    const dispatch = useDispatch();
    const { fullName, username, password } = useSelector((state) => state.auth_reducer);
    const storage = window.localStorage;
    const token = storage.getItem('token') 

    const [rating, setRating] = useState(0);
    const [review, setReview] = useState('')

    //console.log("rating", rating)
    // console.log("ini berhasil")
   // console.log("open", open2, "onCLose", onClose2)
    
    const postReviewProduct = (rating, review) => {
        // console.log("ini postReviewProduct", rating, review)
    }

    //onClose = useState(true)
    //console.log("onclose", onClose)
    return(
    <Popup open={open} closeOnDocumentClick onClose={onClose}>
         <section className={styles.berhasil}>
          <div className={styles.berhasilbox}>
            <PenilaianBerhasil />
            <p className="bold-24">{pesanan}</p>
            <p className="regular-16">{pesananText}</p>
          </div>
        </section>
    </Popup>
    )
}

export default PopUpReviewBerhasil