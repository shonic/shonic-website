import styles from './FotHome.module.scss';
const FotHome = () => {
  return <footer className={`${styles.footer} medium-12`}>Copyright &copy; 2022 Shonic. All rights reserved.</footer>;
};

export default FotHome;
