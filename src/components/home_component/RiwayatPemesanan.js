import styles from './RiwayatPemesanan.module.scss';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import { getOrderList } from '../action';
import CardPesanan from './CardPesanan';
import RemahRoti from './RemahRoti';

const RiwayatPemesanan = () => {
  const dispatch = useDispatch();
  const { riwayatPemesanan } = useSelector((state) => state.checkout_cart_reducer);

  const storage = window.localStorage;
  const token = storage.getItem('token');

  const [status, setStatus] = useState('');
  const [statusFilter, setStatusFilter] = useState('Semua');
  useEffect(() => {
    dispatch(getOrderList(status, token));
  }, []);

  return (
    <>
      <RemahRoti />
      <section className={styles.riwayat}>
        <h3 className={`${styles.riwayat_header} bold-14-res`}>PESANAN SAYA</h3>
        <div className={`${styles.riwayat_search} medium-12-res container`}>
          <p className={styles.riwayat_search_text} onClick={() => setStatusFilter('Semua')}>
            Semua
          </p>
          <p className={styles.riwayat_search_text} onClick={() => setStatusFilter('Belum Bayar')}>
            Belum Bayar
          </p>
          <p
            className={styles.riwayat_search_text}
            onClick={() => {
              setStatusFilter('Diproses');
            }}>
            Diproses
          </p>
          <p className={styles.riwayat_search_text} onClick={() => setStatusFilter('Dikirim')}>
            Dikirim
          </p>
          <p className={styles.riwayat_search_text} onClick={() => setStatusFilter('Selesai')}>
            Selesai
          </p>
          <p className={styles.riwayat_search_text} onClick={() => setStatusFilter('Dibatalkan')}>
            Dibatalkan
          </p>
        </div>
      </section>
      {riwayatPemesanan.length > 0 && riwayatPemesanan !== null ? (
        <section className="container">
          <div className={styles.card_wrapper}>
            <CardPesanan pesanan={riwayatPemesanan} status={statusFilter} />
          </div>
        </section>
      ) : (
        <section className={`${styles.notfound} container`}>
          <img className={styles.image} src="https://i.postimg.cc/C5P5Zh4m/ilustrasi-belum-ada-pesanan.png" alt="pesanan belum ada" />
          <div className={styles.flex}>
            <p className="bold-24">Belum ada pesanan</p>
            <p className="medium-12-res">Anda belum pernah melakukan transaksi pada aplikasi Shonic, ayo mulai berbelanja!</p>
            <Link to={'/'}>
              <button className={styles.btn}>Kembali ke Home</button>
            </Link>
          </div>
        </section>
      )}
    </>
  );
};

export default RiwayatPemesanan;
