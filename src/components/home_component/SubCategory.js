import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { fetchAll } from '../action';
import CardWithSubId from './CardWithSubId';

const SubCategory = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAll());
  }, [dispatch]);

  const { listAll, loading, error } = useSelector((state) => state.homepage_reducer);

  const params = useParams();

  return (
    <>
      <div className="">CATEGORY MASUK</div>
      {listAll !== null && listAll.length > 0 && !loading && !error ? <CardWithSubId products={listAll} idParam={params} /> : null}
    </>
  );
};

export default SubCategory;
