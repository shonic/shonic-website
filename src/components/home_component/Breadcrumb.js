import styles from './Breadcrumb.module.scss';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';

const Breadcrumb = (props) => {
  const { links } = props;
  const breadcrumbs = [
    <Link underline="hover" fontWeight="medium" color="#505050" key="1" href={links[0]}>
      Home
    </Link>,
    <Link underline="hover" key="2" color="#505050" fontWeight="medium" href={links[1]}>
      Hasil Pencarian
    </Link>,
  ];

  return (
    <>
      <div className={styles.breadcrumbs}>
        <div className={`${styles.breadcrumb} container`}>
          <Stack spacing={2}>
            <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
              {breadcrumbs}
            </Breadcrumbs>
          </Stack>
        </div>
      </div>
    </>
  );
};

export default Breadcrumb;
