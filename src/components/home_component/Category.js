import styles from './Brand.module.scss';
import no_product from '../../images/pictures/no_product.png';
import Checkbox from '@mui/material/Checkbox';
import Card from './Card';
import RemahRoti from './RemahRoti';
import { FaStar } from 'react-icons/fa';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { searchByCategory, refreshFilter } from '../action';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

const Category = () => {
  const dispatch = useDispatch();
  const history = useNavigate();
  const { listSearch, category, listCategoryProducts, searchFilter } = useSelector((state) => state.homepage_reducer);

  const [filterRelevansi, setFilterRelevansi] = useState('Produk Terbaru');
  const [showValue, setShowValue] = useState('Rp. 500.000');
  const [filter, setFilter] = useState(false);

  const [filter_4Star, setFilter_4Star] = useState(false);
  const [maxValue, setMaxValue] = useState(1000000000);
  const [minValue, setMinValue] = useState(0);
  const [filter_onlyDiscount, setFilterOnlyDiscount] = useState(false);
  const [pageNo, setPageNo] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [sort_dateDesc, setSortDateDesc] = useState(false);
  const [sort_priceAsc, setSort_priceAsc] = useState(false);
  const [sort_priceDesc, setSortPriceDesc] = useState(false);

  useEffect(() => {
    dispatch(searchByCategory({ history, category, filter_4Star, maxValue, minValue, filter_onlyDiscount, pageNo, pageSize, sort_dateDesc, sort_priceAsc, sort_priceDesc }));
    dispatch(refreshFilter);
  }, [category, filter_4Star, maxValue, minValue, filter_onlyDiscount, pageNo, pageSize, sort_dateDesc, sort_priceAsc, sort_priceDesc]);

  return (
    <>
      <RemahRoti />
      <div className={`${styles.search_container} container`}>
        <div className={styles.search_wrapper}>
          <div className={styles.search_filter}>
            <h1>FILTER</h1>
            <div className={styles.filter_harga}>
              <p className={`medium-12-14-16`}>Harga</p>
              <div className={styles.filter_dropdown}>
                <button className={`${styles.dropdown} medium-12-14-16`}>
                  <div>{showValue}</div>
                  <div className={styles.isi_dropdown}>
                    <option
                      value="lt100"
                      onClick={(e) => {
                        setMaxValue(99999);
                        setShowValue(`< Rp. 100.000`);
                      }}>
                      &lt; Rp. 100.000
                    </option>
                    <option
                      value="gt100"
                      onClick={(e) => {
                        setMinValue(100000);
                        setMaxValue(500000);
                        setShowValue(`< Rp. 100 rb - Rp. 500 rb`);
                      }}>
                      <div>Rp. 100 rb - Rp. 500 rb</div>
                    </option>
                    <option
                      value="gt500"
                      onClick={(e) => {
                        setMinValue(500000);
                        setMaxValue(5500000);
                        setShowValue(`< Rp. 500 rb - Rp. 5.5 jt`);
                      }}>
                      <div>Rp. 500 rb - Rp. 5.5 jt</div>
                    </option>
                    <option
                      value="gt5000"
                      onClick={(e) => {
                        setMinValue(5500000);
                        setMaxValue(10000000);
                        setShowValue(`< Rp. 5.5 jt - Rp. 10 jt`);
                      }}>
                      <div>Rp. 5.5 jt - Rp. 10 jt</div>
                    </option>
                    <option
                      value="gt1000"
                      onClick={(e) => {
                        setMinValue(10000000);
                        setMaxValue(100000000);
                        setShowValue(`> Rp. 10.000.000`);
                      }}>
                      <div>&gt; Rp. 10.000.000</div>
                    </option>
                  </div>
                </button>
              </div>
            </div>
            <div className={styles.filter_penawaran_penilaian}>
              <div className={styles.filter_penawaran}>
                <p className={`medium-12-14-16`}>Penawaran</p>
                <p className={`regular-12-14-16`} onClick={(e) => setFilterOnlyDiscount(!filter_onlyDiscount)}>
                  <Checkbox {...label} />
                  Sedang Diskon
                </p>
              </div>
              <div className={styles.filter_penilaian}>
                <p className={`medium-12-14-16`}>Penilaian</p>
                <p className={`regular-12-14-16`}>
                  <Checkbox {...label} onClick={(e) => setFilter_4Star(!filter_4Star)} />
                  <FaStar style={{ color: '#ffd43a', fontSize: '12px', position: 'relative', top: '2px' }} /> 4 ke atas
                </p>
              </div>
            </div>
            <button
              className={`${styles.reset_filter} regular-12-14-16`}
              onClick={(e) => {
                setFilter_4Star(false);
                setMaxValue(1000000000);
                setMinValue(0);
                setFilterOnlyDiscount(false);
                setSortDateDesc(false);
                setSort_priceAsc(false);
                setSortPriceDesc(false);
                setFilterRelevansi('Produk Terbaru');
              }}>
              Reset Filter
            </button>
          </div>

          <div className={styles.search_result}>
            <div className={styles.search_result_info}>
              <p className={`${styles.p} medium-12-14-16`}>
                Menampilkan hasil pencarian untuk "<span>{category}</span>"
              </p>
              <div className={styles.relevansi_dropdown}>
                <button className={`${styles.dropdown} medium-12-14-16`}>
                  <div>Urutkan: {filterRelevansi}</div>
                  <div className={styles.isi_dropdown}>
                    <option
                      value="produk_terbaru"
                      onClick={(e) => {
                        setSortDateDesc(true);
                        setSortPriceDesc(false);
                        setSort_priceAsc(false);
                        setFilterRelevansi('Produk Terbaru');
                      }}>
                      Produk Terbaru
                    </option>
                    <option
                      value="harga_terendah"
                      onClick={(e) => {
                        setSortDateDesc(false);
                        setSortPriceDesc(false);
                        setSort_priceAsc(true);
                        setFilterRelevansi('Harga Terendah');
                      }}>
                      Harga Terendah
                    </option>
                    <option
                      value="harga_tertinggi"
                      onClick={(e) => {
                        setSortDateDesc(false);
                        setSortPriceDesc(true);
                        setSort_priceAsc(false);
                        setFilterRelevansi('Harga Tertinggi');
                      }}>
                      Harga Tertinggi
                    </option>
                  </div>
                </button>
              </div>
            </div>
            {listCategoryProducts !== undefined && filter === false && searchFilter === true && listSearch !== null && listCategoryProducts.length !== 0 ? (
              <section className={styles.wrapper}>
                <Card products={listCategoryProducts} />
              </section>
            ) : searchFilter === false ? (
              <div className={`${styles.product_not_found_container} container`}>
                <div className={styles.no_product}>
                  <div className={styles.imgku}>
                    <img className={styles.imglost} src={no_product} alt="product not found" />
                  </div>
                  <div className={styles.no_product__title}>
                    <div className={`${styles.card_miss} bold-16-20-28`}>Produk tidak ditemukan</div>
                    <div className={`${styles.fil} medium-12-res`}>Silakan coba kata kunci lain untuk menemukan produk yang Anda cari</div>
                  </div>
                </div>
              </div>
            ) : (
              <>
                <div className={`${styles.product_not_found_container} container`}>
                  <div className={styles.no_product}>
                    <div className={styles.imgku}>
                      <img className={styles.imglost} src={no_product} alt="product not found" />
                    </div>
                    <div className={styles.no_product__title}>
                      <div className={`${styles.card_miss} bold-16-20-28`}>Produk tidak ditemukan</div>
                      <div className={`${styles.fil} medium-12-res`}>Silakan coba kata kunci lain untuk menemukan produk yang Anda cari</div>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
    // </>
  );
};

export default Category;
