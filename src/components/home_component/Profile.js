// pictures //
import { Clock } from '../../images/pictures/Clock';
import { bca } from '../../images/pictures/BCA.png';
import { DangerButton, EyeClosed, EyeOpen, GoogleIcon } from '../../images/icons/ShonicIcon';
// css //
import styles from './Profile.module.scss';
// icons //
import { MdOutlineContentCopy } from 'react-icons/md';
// react router //
import { Link, useNavigate } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
// necessary dependencies //
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import axios from 'axios';
// actions //
import { getProfile, updateProfile, getMyAddress } from '../action';
// components //
import { formatRupiah } from '../../global_function/GlobalFunction';
import PopUp from '../home_component/PopUp';
import RemahRoti from './RemahRoti';

const Profile = () => {
  const dispatch = useDispatch();
  const { profile } = useSelector((state) => state.auth_reducer);
  const { savedAddress } = useSelector((state) => state.checkout_cart_reducer);

  const storage = window.localStorage;
  const token = storage.getItem('token');

  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);

  const [nama, setName] = useState('');
  const [email, setEmail] = useState('');
  const [nomorHp, setNomor] = useState();
  const [gender, setGender] = useState(0);
  const [birthday, setBirthday] = useState('');
  let change2 = birthday.replace(/-/g, '/');
  let birthdate = change2.substring(8, 10) + change2.substring(4, 8) + change2.substring(0, 4);

  useEffect(() => {
    // refresh verifAcc & emailStatus state value
    dispatch(getProfile(token));
    dispatch(getMyAddress(token));
  }, []);

  //console.log("profile", profile)
  //console.log("change2", change2, "birthdate", birthdate)

  return (
    <div className={styles.outer}>
      <RemahRoti />
      <div className={`container`}>
        <div className={`${styles.profile_container}`}>
          <h3 className={`${styles.button} bold-28`}>AKUN SAYA</h3>
          <div className={`${styles.biodata_diri}`}>
            <h3 className={`${styles} bold-16-res`}>Biodata Diri</h3>
            {profile !== undefined ? (
              <form className={styles.form}>
                <div className={styles.field_container}>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>Nama Lengkap</label>
                    <input className={`${styles.input} regular-14`} type="text" value={nama} placeholder={profile['nama']} name="name" onChange={(e) => setName(e.target.value)} />
                  </div>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>email</label>
                    <input className={`${styles.email} regular-14`} type="email" value={profile['email']} placeholder={profile['email']} name="email" disabled />
                  </div>
                </div>
                <div className={styles.field_container}>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>Nomor Telepon</label>
                    <input className={`${styles.input} regular-14`} type="number" value={nomorHp} placeholder={profile['nomorHp']} onChange={(e) => setNomor(e.target.value)} />
                  </div>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>Tanggal Lahir</label>
                    <input className={`${styles.input} regular-14`} type="date" name="birthday" onChange={(event) => setBirthday(event.target.value)} />
                  </div>
                </div>
                <div className={styles.jenis_kelamin}>
                  <label className={`${styles.label} medium-14`}>Jenis Kelamin</label>
                  <div className={styles.radio_container}>
                    <div className={styles.radio}>
                      <input type="radio" name="gender" value="perempuan" onClick={(e) => setGender(e.target.value)} />
                      <label className={`${styles.label} medium-14`} for="perempuan">
                        Perempuan
                      </label>
                    </div>
                    <div className={styles.radio}>
                      <input type="radio" name="gender" value="laki_laki" onClick={(e) => setGender(e.target.value)} />
                      <label className={`${styles.label} medium-14`} value="laki_laki" for="laki_laki">
                        Laki-Laki
                      </label>
                    </div>
                  </div>
                </div>
                <button
                  className={`${styles.button} semibold-16`}
                  onClick={(e) => {
                    e.preventDefault();
                    dispatch(updateProfile(birthdate, profile['email'], gender, nama, nomorHp, token));
                  }}>
                  Simpan
                </button>
              </form>
            ) : (
              <form className={styles.form}>
                <div className={styles.field_container}>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>Nama Lengkap</label>
                    <input className={`${styles.input} regular-14`} type="text" value="" placeholder="Rachel Anastasia" name="Rachel Anastasia" />
                  </div>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>email</label>
                    <input className={`${styles.email} regular-14`} type="email" value="" placeholder="rachelanastasia@gmail.com" name="email" disabled />
                  </div>
                </div>
                <div className={styles.field_container}>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>Nomor Telepon</label>
                    <input className={`${styles.input} regular-14`} type="number" value="" placeholder="08xx-xxxx-xxxx" name="Rachel Anastasia" />
                  </div>
                  <div className={`${styles.div}`}>
                    <label className={`${styles.label} medium-14`}>Tanggal Lahir</label>
                    <input className={`${styles.input} regular-14`} type="date" name="birthday" />
                  </div>
                </div>
                <div className={styles.jenis_kelamin}>
                  <label className={`${styles.label} medium-14`}>Jenis Kelamin</label>
                  <div className={styles.radio_container}>
                    <div className={styles.radio}>
                      <input type="radio" name="gender" value="perempuan" />
                      <label className={`${styles.label} medium-14`} for="perempuan">
                        Perempuan
                      </label>
                    </div>
                    <div className={styles.radio}>
                      <input type="radio" name="gender" value="laki_laki" />
                      <label className={`${styles.label} medium-14`} for="laki_laki">
                        Laki-Laki
                      </label>
                    </div>
                  </div>
                </div>
                <button className={`${styles.button} semibold-16`}>Simpan</button>
              </form>
            )}
          </div>
          <div className={`${styles.alamat_pengiriman}`}>
            <div className={styles.topside}>
              <h3 className={`${styles} bold-16-res`}>Alamat Pengiriman</h3>
              <p type="button" className={`${styles.ubah} medium-16`} onClick={() => setOpen((o) => !o)}>
                Ubah Alamat
              </p>
            </div>
            {savedAddress !== null ? (
              Object.keys(savedAddress).length > 0 ? (
                <div className={styles.downside}>
                  <p className={`${styles} semibold-16`}>
                    {savedAddress['name']} • {savedAddress['phone']}
                  </p>
                  <p className={`${styles} regular-16`}>
                    {savedAddress['detail']}
                    {savedAddress['detail_address']}, {savedAddress['kecamatan']}, {savedAddress['postalCode']}
                  </p>
                </div>
              ) : null
            ) : null}
          </div>
          <PopUp open={open} onClose={closeModal} />
          {/* <div className={`${styles.password}`}>
              <div className={styles.topside}>
                <h3 className={`${styles} bold-16-res`}>Password</h3>
                <p type="button" className={`${styles.ubah} medium-16`}>Ubah Password</p>
              </div>
            </div> */}
        </div>
      </div>
    </div>
  );
};

export default Profile;
