import styles from './Card.module.scss';
import { Star } from '../../images/icons/Card';
import { bintang, formatRupiah } from '../../global_function/GlobalFunction';
import { Link } from 'react-router-dom';

const Card = (props) => {
  const { products } = props;

  return (
    <>
      <section className={styles.cards}>
        {products.map((list, index) => (
          <Link to={'/search_result/' + list.id} style={{ width: '100%', height: '100%', color: '#1c1c1e' }}>
            <div className={styles.card_container} key={list.id}>
              <div className={styles.card_top}>
                <div className={styles.card_photo} style={{ backgroundImage: `url(${list.image})` }}></div>
                {parseInt(list.discount) >= 1 ? <div className={styles.card_chip}>{list.discount} %OFF</div> : null}
              </div>
              <div className={styles.card_bottom}>
                <div className={`${styles.card_name} regular-10-res`}>{list.name}</div>
                <div className={styles.card_prices}>
                  <div className={`${styles.card_price_main} semibold-12-res`}>{formatRupiah(list.after_discount)}</div>
                  {parseInt(list.discount) > 0 ? <div className={`${styles.card_price_discount} medium-10-res`}>{formatRupiah(list.price)}</div> : null}
                </div>
                <div className={styles.card_rate}>
                  <div className={styles.card_star}>
                    <div>{bintang(list.rating)}</div>
                    <Star className={styles.star} />
                    <p className={`${styles.card_star_rate} regular-10-res`}>
                      {list.rating} • {list.review} Ulasan
                    </p>
                  </div>
                </div>
                <button className={styles.card_button}>LIHAT DETAIL</button>
              </div>
            </div>
          </Link>
        ))}
      </section>
    </>
  );
};

export default Card;
