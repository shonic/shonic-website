import { Breadcrumbs as MUIBreadcrumbs, Link } from '@mui/material';
import { useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { RightArrowBread } from '../../images/icons/Navigation';

const RemahRoti2 = () => {
  const { listId } = useSelector((state) => state.homepage_reducer);
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const pathnames = pathname.split('/').filter(Boolean);

  return (
    <div className="breadmargin">
      <div className="container bread">
        <MUIBreadcrumbs aria-label="breadcrumb" separator={<RightArrowBread />}>
          {pathnames.length ? (
            <Link className="regular-12-res breadcrumb" underline="none" color="#505050" onClick={() => navigate('/')}>
              Home
            </Link>
          ) : (
            <p className="regular-12-res breadcrumb" color="#505050">
              Home
            </p>
          )}
          {pathnames.map((name, index) => {
            const routeTo = `/${pathnames.slice(0, index + 1).join('/')}`;
            const isLast = index === pathnames.length - 1;
            return isLast ? (
              <p className="regular-12-res breadcrumb" color="#505050" key={name}>
                {listId['name']}
              </p>
            ) : (
              <Link className="regular-12-res breadcrumb" underline="none" color="#505050" key={name} onClick={() => navigate(routeTo)}>
                {name === 'search_result' ? 'Hasil Pencarian' : name}
              </Link>
            );
          })}
        </MUIBreadcrumbs>
      </div>
    </div>
  );
};

export default RemahRoti2;
