import styles from './CategoryHome.module.scss';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { searchByCategory, searchByCategoryTes } from '../action';

const CategoryHome = () => {
  const items = [
    { id: 1, name: 'Laptop', url: 'https://i.postimg.cc/WzzswNP5/kategori-laptop.png' },
    { id: 2, name: 'PC/Desktop', url: 'https://i.postimg.cc/Fz6NPMrN/kategori-pc.png' },
    { id: 3, name: 'Handphone', url: 'https://i.postimg.cc/nz0HmsNs/kategori-handphone.png' },
    { id: 4, name: 'Audio/Speaker', url: 'https://i.postimg.cc/J4y1yzxn/kategori-audio.png' },
    { id: 5, name: 'Komponen', url: 'https://i.postimg.cc/QNY8xTpw/kategori-komponen.png' },
    { id: 6, name: 'Aksesoris', url: 'https://i.postimg.cc/tTXq7dvQ/kategori-aksesoris.png' },
  ];

  const [category_name, setKategori] = useState('');
  const [filter_4Star, setFilter_4Star] = useState(false);
  const [maxValue, setMaxValue] = useState(1000000000);
  const [minValue, setMinValue] = useState(0);
  const [filter_onlyDiscount, setFilterOnlyDiscount] = useState(false);
  const [pageNo, setPageNo] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [sort_dateDesc, setSortDateDesc] = useState(false);
  const [sort_priceAsc, setSort_priceAsc] = useState(false);
  const [sort_priceDesc, setSortPriceDesc] = useState(false);

  const { category } = useSelector((state) => state.homepage_reducer);

  const dispatch = useDispatch();
  const history = useNavigate();

  const searchCategory = (payload) => {
    dispatch(searchByCategory({ history, category_name, filter_4Star, maxValue, minValue, filter_onlyDiscount, pageNo, pageSize, sort_dateDesc, sort_priceAsc, sort_priceDesc }));
  };

  return (
    <>
      <section className={styles.kategori}>
        <div className={styles.kategori_rounded}>
          <div className={`${styles.kategori_box} container`}>
            <h3 className={`${styles.kategori_header} bold-14-res`}>KATEGORI</h3>
            <div className="swiffy-slider slider-item-show3 slider-nav-round slider-nav-page slider-help">
              <div className={`${styles.kategori_items} slider-container `} style={{ gridAutoColumns: 'unset' }}>
                {items.map(({ name, url, index }) => (
                  <>
                    <div
                      key={index}
                      className={styles.kategori_item}
                      onClick={() => {
                        dispatch(searchByCategoryTes(history, `${name}`));
                      }}>
                      <div className={styles.kategori_img} style={{ backgroundImage: `url(${url})` }}></div>
                      <p className="medium-12-res">{name}</p>
                    </div>
                  </>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default CategoryHome;
