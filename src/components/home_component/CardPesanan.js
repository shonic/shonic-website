import styles from './RiwayatPemesanan.module.scss';
import { Link } from 'react-router-dom';
import { formatRupiah } from '../../global_function/GlobalFunction';

const CardPesanan = (props) => {
  const { pesanan, status } = props;

  return (
    <>
      {pesanan.map((item, index) => {
        if (status === 'Semua') {
          return (
            <Link to={'/detail_transaksi/' + item.id} style={{ width: '100%', height: '100%' }}>
              <div className={styles.riwayat_main_grid}>
                <div className={styles.image}>
                  <img className={styles.laptop} src={item['product']['image_url']} alt="laptop" />
                  <div className={styles.laptop_text}>
                    <p className="semibold-20-res" style={{ color: '#1c1c1e' }}>
                      {item['product']['name']}
                    </p>
                    <p className="bold-20" style={{ color: '#1c1c1e' }}>
                      {formatRupiah(item['product']['price'])}
                    </p>
                    <p className="regular-10-res" style={{ color: '#1c1c1e' }}>
                      Kuantitas : {item['product']['qty']}
                    </p>
                  </div>
                </div>
                <div className={styles.chip}>
                  {item['status'] === 'Diproses' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #EECEB0', color: '#734011', backgroundColor: '#FFF9F2' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Belum Bayar' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #D2C3EE', color: '#7940CC', backgroundColor: '#F4EFFF' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Dikirim' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #B1C5FF', color: '#3E7BFA', backgroundColor: '#ECF2FF' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Selesai' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #B8DBCA', color: '#20573D', backgroundColor: '#F5F5FF' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Dibatalkan' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #EEB4B0', color: '#CB3A31', backgroundColor: '#FFF4F2' }}>
                      {item['status']}
                    </div>
                  ) : (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #B1C5FF', color: '#3E7BFA', backgroundColor: '#ECF2FF' }}>
                      kosong
                    </div>
                  )}
                </div>
                <div className={styles.price}>
                  <p className="regular-14-res" style={{ color: '#1c1c1e' }}>
                    Total Pesanan:{' '}
                    <span className="bold-24-res" style={{ color: '#1c1c1e' }}>
                      {formatRupiah(item['total_price'])}
                    </span>
                  </p>
                </div>
                <div className={`${styles.info} regular-14-res`}>
                  {item.status === 'Belum Bayar' ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Bayar Pesanan Sebelum :</p>
                      <p className={`${styles} bold-14`}>
                        <span style={{ color: '#3E7BFA' }}>{item.deadline}</span>
                      </p>
                    </>
                  ) : item.status === 'Diproses' ? (
                    <p style={{ color: '#1c1c1e' }}>{item.status_detail}</p>
                  ) : item.status === 'Dikirim' ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Estimasi Pengiriman :</p>
                      <p className={`${styles} bold-14`}>
                        <span style={{ color: '#3E7BFA' }}>{item.estimate}</span>
                      </p>
                    </>
                  ) : item.status === 'Selesai' && item.reviewed === false ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Nilai Pesanan Sebelum</p>
                      <p className={`${styles} bold-14`}>
                        <span style={{ color: '#3E7BFA' }}>{item.deadline}</span>
                      </p>
                    </>
                  ) : item.status === 'Selesai' && item.reviewed === true ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Pesanan Telah Dinilai</p>
                    </>
                  ) : item.status === 'Dibatalkan' ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>{item['status_detail']}</p>
                    </>
                  ) : null}
                </div>
                {item.status === 'Belum Bayar' ? (
                  <Link to={`/pembayaran/${item.id}`} className={styles.button}>
                    <button className={`${styles.button_buy} semibold-14-res`}>Bayar Sekarang</button>
                  </Link>
                ) : item.status === 'Diproses' ? (
                  <div className={styles.button}>
                    <button className={`${styles.button_proses} semibold-14-res`}>Pesanan Diterima</button>
                  </div>
                ) : item.status === 'Dikirim' ? (
                  <div className={styles.button}>
                    <button className={`${styles.button_buy} semibold-14-res`}>Pesanan Diterima</button>
                  </div>
                ) : item.status === 'Selesai' && item.reviewed === false ? (
                  <div className={styles.button}>
                    <button className={`${styles.button_buy} semibold-14-res`}>Beri Penilaian</button>
                  </div>
                ) : item.status === 'Selesai' && item.reviewed === true ? (
                  <Link to={`/search_result/${item['product']['id']}`} className={styles.button}>
                    <div className={styles.button}>
                      <button className={`${styles.button_buy} semibold-14-res`}>Beli Lagi</button>
                    </div>
                  </Link>
                ) : item.status === 'Dibatalkan' ? (
                  <Link to={`/search_result/${item['product']['id']}`} className={styles.button}>
                    <div className={styles.button}>
                      <button className={`${styles.button_buy} semibold-14-res`}>Beli Lagi</button>
                    </div>
                  </Link>
                ) : null}
              </div>
            </Link>
          );
        } else if (status !== 'Semua' && item.status === status) {
          return (
            <Link to={'/detail_transaksi/' + item.id} style={{ width: '100%', height: '100%' }}>
              <div className={styles.riwayat_main_grid}>
                <div className={styles.image}>
                  <img className={styles.laptop} src={item['product']['image_url']} alt="laptop" />
                  <div className={styles.laptop_text}>
                    <p className="semibold-20-res" style={{ color: '#1c1c1e' }}>
                      {item['product']['name']}
                    </p>
                    <p className="bold-20" style={{ color: '#1c1c1e' }}>
                      {formatRupiah(item['product']['price'])}
                    </p>
                    <p className="regular-10-res" style={{ color: '#1c1c1e' }}>
                      Kuantitas : {item['product']['qty']}
                    </p>
                  </div>
                </div>
                <div className={styles.chip}>
                  {item['status'] === 'Diproses' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #EECEB0', color: '#734011', backgroundColor: '#FFF9F2' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Belum Bayar' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #D2C3EE', color: '#7940CC', backgroundColor: '#F4EFFF' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Dikirim' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #B1C5FF', color: '#3E7BFA', backgroundColor: '#ECF2FF' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Selesai' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #B8DBCA', color: '#20573D', backgroundColor: '#F5F5FF' }}>
                      {item['status']}
                    </div>
                  ) : item['status'] === 'Dibatalkan' ? (
                    <div className={`${styles.chipbox} semibold-16-res`} style={{ border: '1px solid #EEB4B0', color: '#CB3A31', backgroundColor: '#FFF4F2' }}>
                      {item['status']}
                    </div>
                  ) : null}
                </div>
                <div className={styles.price}>
                  <p className="regular-14-res" style={{ color: '#1c1c1e' }}>
                    Total Pesanan:{' '}
                    <span className="bold-24-res" style={{ color: '#1c1c1e' }}>
                      {formatRupiah(item['total_price'])}
                    </span>
                  </p>
                </div>
                <div className={`${styles.info} regular-14-res`}>
                  {item.status === 'Belum Bayar' ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Bayar Pesanan Sebelum :</p>
                      <p className={`${styles} bold-14`}>
                        <span style={{ color: '#3E7BFA' }}>{item.deadline}</span>
                      </p>
                    </>
                  ) : item.status === 'Diproses' ? (
                    <p style={{ color: '#1c1c1e' }}>{item.status_detail}</p>
                  ) : item.status === 'Dikirim' ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Estimasi Pengiriman :</p>
                      <p className={`${styles} bold-14`}>
                        <span style={{ color: '#3E7BFA' }}>{item.estimate}</span>
                      </p>
                    </>
                  ) : item.status === 'Selesai' && item.reviewed === false ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Nilai Pesanan Sebelum</p>
                      <p className={`${styles} bold-14`}>
                        <span style={{ color: '#3E7BFA' }}>{item.deadline}</span>
                      </p>
                    </>
                  ) : item.status === 'Selesai' && item.reviewed === true ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>Pesanan telah dinilai</p>
                    </>
                  ) : item.status === 'Dibatalkan' ? (
                    <>
                      <p style={{ color: '#1c1c1e' }}>{item['status_detail']}</p>
                    </>
                  ) : null}
                </div>
                {item.status === 'Belum Bayar' ? (
                  <Link to={`/pembayaran/${item.id}`} className={styles.button}>
                    <button className={`${styles.button_buy} semibold-14-res`}>Bayar Sekarang</button>
                  </Link>
                ) : item.status === 'Diproses' ? (
                  <div className={styles.button}>
                    <button className={`${styles.button_proses} semibold-14-res`}>Pesanan Diterima</button>
                  </div>
                ) : item.status === 'Dikirim' ? (
                  <div className={styles.button}>
                    <button className={`${styles.button_buy} semibold-14-res`}>Pesanan Diterima</button>
                  </div>
                ) : item.status === 'Selesai' && item.reviewed === false ? (
                  <div className={styles.button}>
                    <button className={`${styles.button_buy} semibold-14-res`}>Beri Penilaian</button>
                  </div>
                ) : item.status === 'Selesai' && item.reviewed === true ? (
                  <div className={styles.button}>
                    <button className={`${styles.button_buy} semibold-14-res`}>Beli Lagi</button>
                  </div>
                ) : item.status === 'Dibatalkan' ? (
                  <Link to={`/search_result/${item['product']['id']}`} className={styles.button}>
                    <div className={styles.button}>
                      <button className={`${styles.button_buy} semibold-14-res`}>Beli Lagi</button>
                    </div>
                  </Link>
                ) : null}
              </div>
            </Link>
          );
        }
      })}
    </>
  );
};

export default CardPesanan;
