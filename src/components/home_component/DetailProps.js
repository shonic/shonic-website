import { bintang, formatRupiah } from '../../global_function/GlobalFunction';
import { Check, Decrement, Increment, Like, Star } from '../../images/icons/Card';
import Card from './Card';
import styles from './DetailResult.module.scss';

const DetailProps = ({ listId, list, loading, error }) => {
  return (
    <>
      <div className={`${styles.detail_page} container`}>
        <div className={styles.detail_img} style={{ backgroundImage: `url(${listId.image})` }}></div>
        <div className={styles.detail_desc}>
          <div className={styles.detail_desc_top}>
            <div className={styles.desc_top_flex}>
              <div className={`${styles.desc_top_1} font-detail`}>{listId.name}</div>
              <div className={styles.desc_top_2}>
                {listId.discount !== 0 ? <div className={`${styles.harga} font-detail-price`}>{formatRupiah(listId.after_discount)}</div> : <div className={`${styles.harga} font-detail-price`}>{formatRupiah(listId.price)}</div>}
                {listId.discount !== 0 && <div className={`${styles.disc} medium-12-res`}>{formatRupiah(listId.price)}</div>}
                {listId.discount !== 0 && <div className={`${styles.discchip} font-detail-chip`}>{listId.discount}% OFF</div>}
              </div>
              <div className={styles.desc_top_3}>
                <div className={`${styles.terjual} regular-12-res`}>terjual 18</div>
                <Star className={styles.star1} />
                <div className={styles.star5}>{bintang(listId.rating)}</div>
                <div className={`${styles.ulasan} regular-12-res`}>| {listId.review} ulasan</div>
              </div>
            </div>
            <div className={styles.detail_desc_mid}>
              <Check className={styles.check} />
              <div>
                <p className="font-original">Produk Original 100%</p>
                <p className="regular-10-res">Garansi produk resmi official store</p>
              </div>
            </div>
            <div className={styles.detail_desc_bot}>
              <p className="semibold-16-res">Rincian Produk</p>
              <div className={`${styles.detail_desc_flex} regular-12-res`}>
                <div className={styles.detail_rincian}>
                  <p className={styles.detail_desc_text}>Stok</p>
                  <p className={styles.detail_desc_text}>Merk</p>
                  <p className={styles.detail_desc_text}> Kategori</p>
                </div>
                <div className={`${styles.detail_detail} regular-12-res`}>
                  <p className={styles.detail_desc_text}>{listId.qty}</p>
                  <p className={styles.detail_desc_text}>{listId.brand.name}</p>
                  <p className={styles.detail_desc_text}>
                    {listId.category.category_parent.name} - {listId.category.name}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className={styles.addtocart_wrapper}>
        <div className={`${styles.addtocart_sticky} container`}>
          <div className={`${styles.addtocart_x} semibold-16`}>{listId.name}</div>
          <div className={styles.addtocart_flex}>
            <button className={styles.addtocart_increment}>
              <Decrement className={styles.addtocart_increment_svg} />
            </button>
            <div className="semibold-12-res">1/ngerender angka as a state</div>
            <button className={styles.addtocart_increment}>
              <Increment className={styles.addtocart_increment_svg} />
            </button>
          </div>
          <div className={styles.addtocart_price}>
            <p className="medium-12-res">Total</p>
            <p className="bold-16-res">{formatRupiah(listId.price)}</p>
          </div>
          <button className={styles.addtocart_y}>
            <Like className={styles.y_like} />
            <div className={styles.wishlist}>Add To Wishlist</div>
          </button>
          <button className={styles.addtocart_button}>Add To Cart</button>
        </div>
      </section>
      <div className="container">
        <div className={styles.deskripsi}>
          <div className={`${styles.deskripsi_flex} semibold-16-res`}>
            <div className="">Deskripsi Produk</div>
            <div className="">Rating dan Ulasan</div>
          </div>
          <pre className={`${styles.deskripsi_text} regular-12-res`}>{listId.description}</pre>
        </div>
      </div>
      {!loading && !error && (
        <section className="container">
          <p className={`${styles.mungkin_suka} bold-16-res`}>Mungkin Anda Suka</p>
          <Card products={list} />
        </section>
      )}
    </>
  );
};

export default DetailProps;
