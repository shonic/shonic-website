import styles from './Discount.module.scss';
import Card from './Card';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getFlashSale } from '../action';
import RemahRoti from './RemahRoti';

export default function Discount() {
  const dispatch = useDispatch();
  const { listFlash, error, loading } = useSelector((state) => state.homepage_reducer);
  const breadcrumbs = ['/'];

  useEffect(() => {
    const interval = setInterval(() => {
      dispatch(getFlashSale());
    }, 1000);
    return () => clearInterval(interval);
  }, [dispatch]);

  const splitJam = (x) => {
    return x.split(':')[0];
  };
  const splitMenit = (x) => {
    return x.split(':')[1];
  };
  const splitDetik = (x) => {
    return x.split(':')[2];
  };

  return (
    <>
      {listFlash !== null && listFlash !== undefined && Object.keys(listFlash).length > 0 ? (
        <>
          <RemahRoti />
          <div className={styles.search_container}>
            <div className={styles.no_product}>
              <img className={styles.no_product_img} src="https://i.postimg.cc/kGjY7M2T/disc.jpg" alt="" />
            </div>
            {listFlash.length !== 0 ? (
              <div className={styles.product_not_found_container}>
                <div className={`${styles.no_product_recommendation} container`}>
                  <div className={styles.no_product_recommendation_text}>
                    <div className={styles.no_product_recommendation_left}>
                      <h3 className={`${styles.header} bold-14-res`}>FLASH SALE</h3>
                      <p className={`${styles.p} medium-14`}>Berakhir dalam</p>
                      <div className={styles.buttons}>
                        <button className={styles.button}>{splitJam(listFlash.countdown)}</button> :<button className={styles.button}>{splitMenit(listFlash.countdown)} </button>:
                        <button className={styles.button}> {splitDetik(listFlash.countdown)}</button>
                      </div>
                    </div>
                  </div>
                  {!loading && !error && (
                    <section>
                      <Card products={listFlash.products} />
                    </section>
                  )}
                </div>
              </div>
            ) : (
              <div className=""> </div>
            )}
          </div>
        </>
      ) : (
        <div className=""> </div>
      )}
    </>
  );
}
