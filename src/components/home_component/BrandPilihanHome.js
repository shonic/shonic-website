import styles from './BrandPilihanHome.module.scss';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { searchByBrandTes } from '../action';

const BrandPilihanHome = () => {
  const dispatch = useDispatch();
  const history = useNavigate();
  return (
    <>
      <div className={styles.pilihan_box}>
        <div className={styles.pilihan}>
          <div className={styles.pilihan_rounded}>
            <div className={`${styles.pilihan_box} container`}>
              <h3 className={`${styles.pilihan_header} bold-14-res`}>BRAND PILIHAN</h3>
              <div className={`${styles.slider_container}`}>
                <div className="swiffy-slider slider-item-show3 slider-nav-round slider-nav-page slider-help">
                  <ul className="slider-container" style={{ gridAutoColumns: 'unset' }}>
                    <li>
                      <div className={styles.pilihan_item1}>
                        <div
                          className={styles.pilihan_img}
                          style={{ backgroundImage: `url(https://i.postimg.cc/BvWgsRY5/pilihan1.png)` }}
                          onClick={() => {
                            dispatch(searchByBrandTes(history, 'samsung'));
                          }}></div>
                      </div>
                    </li>
                    <li>
                      <div className={styles.pilihan_item2}>
                        <div
                          className={styles.pilihan_img}
                          style={{ backgroundImage: `url(https://i.postimg.cc/yYdnTm3R/pilihan2.png)` }}
                          onClick={() => {
                            dispatch(searchByBrandTes(history, 'asus'));
                          }}></div>
                      </div>
                    </li>
                    <li>
                      <div className={styles.pilihan_item3}>
                        <div
                          className={styles.pilihan_img}
                          style={{ backgroundImage: `url(https://i.postimg.cc/WzSW1SFN/pilihan3.png)` }}
                          onClick={() => {
                            dispatch(searchByBrandTes(history, 'acer'));
                          }}></div>
                      </div>
                    </li>
                    <li>
                      <div className={styles.pilihan_item4}>
                        <div
                          className={styles.pilihan_img}
                          style={{ backgroundImage: `url(https://i.postimg.cc/xTthfjMv/pilihan4.png)` }}
                          onClick={() => {
                            dispatch(searchByBrandTes(history, 'iphone'));
                          }}></div>
                      </div>
                    </li>
                    <li>
                      <div className={styles.pilihan_item5}>
                        <div
                          className={styles.pilihan_img}
                          style={{ backgroundImage: `url(https://i.postimg.cc/rwsbK6Nv/pilihan5.png)` }}
                          onClick={() => {
                            dispatch(searchByBrandTes(history, 'logitech'));
                          }}></div>
                      </div>
                    </li>
                    <li>
                      <div className={styles.pilihan_item6}>
                        <div
                          className={styles.pilihan_img}
                          style={{ backgroundImage: `url(https://i.postimg.cc/tJzfVpfH/pilihan6.png)` }}
                          onClick={() => {
                            dispatch(searchByBrandTes(history, 'hp'));
                          }}></div>
                      </div>
                    </li>
                    <li>
                      <div className={styles.pilihan_item7}>
                        <div
                          className={styles.pilihan_img}
                          style={{ backgroundImage: `url(https://i.postimg.cc/fW71Pfbg/pilihan7.png)` }}
                          onClick={() => {
                            dispatch(searchByBrandTes(history, 'mi'));
                          }}></div>
                      </div>
                    </li>
                    <li></li>
                  </ul>
                  <button type="button" className="slider-nav" aria-label="Go left"></button>
                  <button type="button" className="slider-nav slider-nav-next" aria-label="Go left"></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BrandPilihanHome;
