import axios from 'axios';
const fetchRefreshState = {
  type: 'fetch-refresh',
};
const logSucess = () => {
  return {
    type: 'LOG_SUCCESS',
  };
};
const LogFailed = () => {
  return {
    type: 'LOG_FAILED',
  };
};
const fetchRefreshStatePesanan = {
  type: 'fetchPesanan-refresh',
};
const loginActionAsync = (email, password, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/auth/login`, {
        email,
        password,
      })
      .then((response) => {
        const token = response.data.token;
        const storage = window.localStorage;
        storage.setItem('token', token);
        dispatch(loginActionSuccess({ email, password }));
        history('/');
        dispatch(logSucess());
      })
      .catch((error) => {
        dispatch(LogFailed());
      });
  };
};

const loginActionSuccess = (payload) => ({
  type: 'login/success',
  status: 'success',
  payload,
});

const registAccountAsync = (email, fullname, password, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/auth/register`, {
        email,
        fullname,
        password,
      })
      .then((response) => {
        dispatch(registActionSuccess(fullname));
        history('/login');
      })
      .catch((error) => {
        console.log('error', error);
      });
  };
};

const registActionSuccess = (payload) => (
  console.log('payload register: ', payload),
  {
    type: 'regist/success',
    status: 'success',
    payload,
  }
);

const registAccountAsyncWithLoading = (email, fullname, password, history) => {
  return (dispatch, getState, baseUrl) => {
    dispatch(fetchRefreshState);
    axios
      .post(
        `${baseUrl}/api/v1/auth/register`,
        {
          email,
          fullname,
          password,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then((response) => {
        history('/login');
        dispatch(loginActionSuccess({ email, password }));
      })
      .catch((error) => {});
  };
};

const checkEmailAsync = (email, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`https://shonic-test.herokuapp.com/api/v1/otp/send`, {
        email,
      })
      .then((response) => {
        if (response.status === 200) {
          //// console.log("email sudah terdaftar")
          dispatch(checkEmailSucces(email));
          history('/verifikasi');
        } else {
        }
      })
      .catch((error) => {
        dispatch(checkEmailFailed);
      });
  };
};

const checkEmailSucces = (payload) => ({
  type: 'check-email/success',
  payload,
});

const checkEmailFailed = {
  type: 'check-email/failed',
};

const otpVerifAsync = (email, otp, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/otp/validate`, {
        email,
        otp,
      })
      .then((response) => {
        history('/lengkapi_pendaftaran');
        dispatch(otpVerifSuccess);
        dispatch(logSucess());
      })
      .catch((error) => {
        dispatch(otpVerifFail);
        dispatch(LogFailed());
      });
  };
};

const otpVerifSuccess = {
  type: 'otp-verif/success',
};

const otpVerifFail = {
  type: 'otp-verif/fail',
};

const checkEmailForgotPass = (email, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/forgotpassword/send`, {
        email,
      })
      .then((response) => {
        dispatch(checkEmailSucces(email));
        history('/resetpass_verif');
      })
      .catch((error) => {
        dispatch(checkEmailFailed);
      });
  };
};

const otpVerifForgotPass = (email, otp, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/forgotpassword/validate`, {
        email,
        otp,
      })
      .then((response) => {
        dispatch(otpVerifPassSuccess(response.data.token));
        history('/resetpass_next');
      })
      .catch((error) => {
        dispatch(otpVerifPassFail);
      });
  };
};

const createNewPass = (email, newPass, tokenPass, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/forgotpassword/reset_password`, {
        email: `${email}`,
        newPassword: newPass,
        token: tokenPass,
      })
      .then((response) => {
        dispatch(createNewPassSuccess(newPass));
        history('/resetpass_done');
      })
      .catch((error) => {
        dispatch(otpVerifPassFail);
      });
  };
};

const otpVerifPassSuccess = (payload) => ({
  type: 'otp-verif-forgotPass/success',
  payload,
});

const otpVerifPassFail = {
  type: 'otp-verif-forgotPass/fail',
};

const createNewPassSuccess = (payload) => ({
  type: 'create-new-pass/success',
  payload,
});

const getAllProduct = () => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/product/getAllLatestProduct?pageNo=1&pageSize=200`, {})
      .then((response) => {
        dispatch(fetchProductSuccess(response.data.data));
      })
      .catch((error) => {
        dispatch(fetchProductFailed(error));
      });
  };
};

const fetchProductSuccess = (payload) => ({
  type: 'fetch-product-success',
  payload,
});

const fetchProductFailed = (payload) => ({
  type: 'fetch-product-failed',
  payload,
});

const searchProductApi = (props, history) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/product/search`, {
        params: { keyword: props },
      })
      .then((response) => {
        if (response.data.data.found === true) {
          dispatch(searchProductSuccess(response.data.data.product, props));
          history('/search_result');
        } else {
          dispatch(searchProductFailed(response.data.data.product));
          history('/product_not_found');
        }
      })
      .catch((error) => {
        dispatch(searchProductFailed(error));
        history('/product_not_found');
      });
  };
};

const searchProductSuccess = (payload, keyword) => ({
  type: 'search-product-success',
  payload,
  keyword,
});

const searchProductFailed = (payload) => ({
  type: 'search-product-failed',
  payload,
});

const getFilter = (props) => {
  const { history, keyword, filter_4Star, maxValue, minValue, filter_onlyDiscount, pageNo, pageSize, sort_dateDesc, sort_priceAsc, sort_priceDesc } = props;
  return (dispatch, getState, baseUrl) => {
    axios
      .get(
        `${baseUrl}/api/v1/product/search?filter_4Star=${filter_4Star}&filter_maxPrice=${maxValue}&filter_minPrice=${minValue}&filter_onlyDiscount=${filter_onlyDiscount}&keyword=${keyword}&pageNo=${pageNo}&pageSize=10&sort_dateDesc=${sort_dateDesc}&sort_priceAsc=${sort_priceAsc}&sort_priceDesc=${sort_priceDesc}`
      )
      .then((response) => {
        if (response.data.data.found === true) {
          dispatch(searchProductSuccess(response.data.data.product, keyword));
        } else {
          dispatch(searchFilterFailed(response.data.data.product));
        }
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const refreshFilter = (payload) => ({
  type: 'refresh-filter',
  payload,
});

const searchFilterFailed = (payload) => ({
  type: 'search-filter-failed',
  payload,
});

const getFamiliar = (id) => {
  return (dispatch, getState, baseUrl) => {
    //// console.log('data dummy');
    axios
      .get(`${baseUrl}/api/v1/product/simmilar/${id}`, {})
      .then((response) => {
        dispatch(fetchFamiliarSuccess(response.data.data));
        // // console.log('🚀 ~ file: action.js ~ line 293 ~ .then ~ response.data.data', response.data.data);
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const fetchFamiliarSuccess = (payload) => ({
  type: 'fetch-familiar-success',
  payload,
});

const searchByBrandTes = (history, brand_name) => {
  return (dispatch, getState, baseUrl) => {
    dispatch(searchBrandSuccessTes(history, brand_name));
    history(`/brand`);
  };
};

const searchBrandSuccessTes = (payload, brand_name) => ({
  type: 'search-brand-success',
  brand_name,
});

const searchByCategory = (props) => {
  const { history, category, filter_4Star, maxValue, minValue, filter_onlyDiscount, pageNo, pageSize, sort_dateDesc, sort_priceAsc, sort_priceDesc } = props;
  return (dispatch, getState, baseUrl) => {
    axios
      .get(
        `${baseUrl}/api/v1/product/getAllByCategory?category_name=${category}&filter_4Star=${filter_4Star}&filter_maxPrice=${maxValue}&filter_minPrice=${minValue}&filter_onlyDiscount=${filter_onlyDiscount}&pageNo=${pageNo}&pageSize=${pageSize}&sort_dateDesc=${sort_dateDesc}&sort_priceAsc=${sort_priceAsc}&sort_priceDesc=${sort_priceDesc}`
      )
      .then((response) => {
        if (response.data.data !== null) {
          dispatch(searchCategorySuccess(response.data.data, category));
        }
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const searchCategorySuccess = (payload, category_name) => ({
  type: 'search-category-success',
  payload,
  category_name,
});

const searchByCategoryTes = (history, category_name) => {
  return (dispatch, getState, baseUrl) => {
    dispatch(searchCategorySuccessTes(history, category_name));
    history(`/category`);
  };
};

const searchCategorySuccessTes = (payload, category_name) => ({
  type: 'search-category-success',
  category_name,
});

const searchByBrand = (props) => {
  const { history, brand, filter_4Star, maxValue, minValue, filter_onlyDiscount, pageNo, pageSize, sort_dateDesc, sort_priceAsc, sort_priceDesc } = props;
  return (dispatch, getState, baseUrl) => {
    axios
      .get(
        `${baseUrl}/api/v1/product/getAllByBrand?brand_name=${brand}&filter_4Star=${filter_4Star}&filter_maxPrice=${maxValue}&filter_minPrice=${minValue}&filter_onlyDiscount=${filter_onlyDiscount}&pageNo=${pageNo}&pageSize=${pageSize}&sort_dateDesc=${sort_dateDesc}&sort_priceAsc=${sort_priceAsc}&sort_priceDesc=${sort_priceDesc}`
      )
      .then((response) => {
        if (response.data.data !== null) {
          dispatch(searchBrandSuccess(response.data.data, brand));
        }
      })
      .catch((error) => {});
  };
};

const searchBrandSuccess = (payload, brand_name) => ({
  type: 'search-brand-success',
  payload,
  brand_name,
});

const getBrandImage = (brand) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/brand/getImage?name_brand=${brand}`)
      .then((response) => {
        if (response.data.data !== null) {
          dispatch(getBrandImageSuccess(response.data.data));
        }
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const getBrandImageSuccess = (payload) => ({
  type: 'get-brand-image',
  payload,
});

const getProductById = (id) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/product/getById/${id}`, {})
      .then((response) => {
        dispatch(fetchIdSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
        dispatch(fetchIdFailed(error));
      });
  };
};

const fetchIdSuccess = (payload) => ({
  type: 'fetch-id-success',
  payload,
});

const fetchIdFailed = (payload) => ({
  type: 'fetch-id-failed',
  payload,
});

const incrementItem = (payload) => ({
  type: 'increment-item',
  payload,
});
const decrementItem = (payload) => ({
  type: 'decrement-item',
  payload,
});
const incrementCart = (payload) => ({
  type: 'increment-cart',
  payload,
});
const decrementCart = (payload) => ({
  type: 'decrement-cart',
  payload,
});
const addProductToCart = (ID, detailTotal, detailQuantity, hargaBarang, history) => {
  return (dispatch, getState, baseUrl) => {
    dispatch(addToCart(ID, detailTotal, detailQuantity, hargaBarang));
    history(`/checkout/`);
  };
};
const addToCart = (ID, detailTotal, detailQuantity, hargaBarang) => ({
  ID,
  detailTotal,
  detailQuantity,
  hargaBarang,
  type: 'add-to-cart',
  payload: ID,
});
const removeFromCart = (payload) => ({
  type: 'remove-from-cart',
  payload,
});
const deleteCart = (payload) => ({
  type: 'delete-cart',
  payload,
});

const getAllCities = () => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/address/cities`, {})
      .then((response) => {
        dispatch(fetchAllCitiesSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
        //dispatch(fetchProductFailed(error));
      });
  };
};

const fetchAllCitiesSuccess = (payload) => ({
  type: 'fetch-allCities-success',
  payload,
});

const getAllProvinces = () => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/address/provinces`, {})
      .then((response) => {
        dispatch(fetchProvincesSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
        //dispatch(fetchProductFailed(error));
      });
  };
};

const fetchProvincesSuccess = (payload) => ({
  type: 'fetch-provinces-success',
  payload,
});

const getAllCitiesByProvince = (id) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(
        `${baseUrl}/api/v1/address/province/${id}/cities`,
        {},
        {
          headers: {
            Authorization: `Basic`,
          },
        }
      )
      .then((response) => {
        dispatch(fetchCitiesByProvincesSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
        //dispatch(fetchProductFailed(error));
      });
  };
};

const fetchCitiesByProvincesSuccess = (payload) => ({
  type: 'fetch-citiesByProvince-success',
  payload,
});

const saveAddress = (city_id, detail, kecamatan, name, phone, postal_code, province_id, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/address/save?token=${token}`, { city_id, detail, kecamatan, name, phone, postal_code, province_id })
      .then((response) => {
        dispatch(getMyAddress(token));
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const saveAddressSuccess = (payload) =>
  // // console.log("payload saveAddressSuccess", payload),
  ({
    type: 'post-saveAddress-success',
    payload,
  });

const getMyAddress = (token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/address/myaddress?token=${token}`)
      .then((response) => {
        // // console.log('response data myaddress: ', response.data.data);
        dispatch(saveAddressSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const getShippingCost = (cart, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/checkout/cost?token=${token}`, {
        products: [
          {
            productId: `${cart['id']}`,
            qty: cart['jumlah'],
          },
        ],
      })
      .then((response) => {
        dispatch(getShippingCostSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error1', error);
      });
  };
};

const getShippingCostButton = (cart, subQuantity, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/checkout/cost?token=${token}`, {
        products: [
          {
            productId: `${cart}`,
            qty: `${subQuantity}`,
          },
        ],
      })
      .then((response) => {
        dispatch(getShippingCostSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const getShippingCostSuccess = (payload) =>
  // // console.log("payload getShippingCostSuccess", payload),
  ({
    type: 'shippingCost-success',
    payload,
  });

const getAllAccount = () => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/account/all`, {})
      .then((response) => {
        //console.log('response data: ', response.data.data);
        dispatch(getAllAccountSuccess(response.data.data));
        // // console.log('🚀 ~ file: action.js ~ line 293 ~ .then ~ response.data.data', response.data.data);
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const getAllAccountSuccess = (payload) =>
  // // console.log('payload: ', payload),
  ({
    type: 'account-success',
    payload,
  });

const onCheckOut = (total, history, token, code, service, note, payment_account_id, productId, qty) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/checkout/create?token=${token}`, {
        courier: {
          code: `${code}`,
          service: `${service}`,
        },
        note: `${note}`,
        payment_account_id: payment_account_id,
        products: {
          productId: `${productId}`,
          qty: qty,
        },
      })
      .then((response) => {
        if (response.data.status === 201) {
          dispatch(onCheckOutSuccess(total));
          history(`/pembayaran/${response.data.data['order_id']}`);
        }
      })
      .catch((error) => {
        //console.log('error', error);
      });
  };
};

const onCheckOutSuccess = (payload) => ({
  type: 'onCheckOut-success',
  payload,
});

const getPaymentData = (id, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/order/${id}/payment?token=${token}`, {})
      .then((response) => {
        dispatch(getPaymentDataSuccess(response.data.data));
        // // console.log('🚀 ~ file: action.js ~ line 293 ~ .then ~ response.data.data', response.data.data);
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const getPaymentDataSuccess = (payload) => ({
  type: 'getPaymentData-success',
  payload,
});

const postUploadPembayaran = (bank_name, bank_number, id, name, photo, total_transfer, token, history) => {
  let formData = new FormData();
  formData.append('bank_name', `${bank_name}`);
  formData.append('bank_number', `${bank_number}`);
  formData.append('id', id);
  formData.append('name', `${name}`);
  formData.append('photo', photo);
  formData.append('total_transfer', total_transfer);

  for (var key of formData.entries()) {
  }

  const url = `https://shonic-test.herokuapp.com/api/v1/order/${id}/payment?token=${token}`;
  const config = {
    headers: { 'Content-Type': 'multipart/form-data' },
  };
  return (dispatch, getState, baseUrl) => {
    axios
      .post(url, formData, config)
      .then((response) => {
        if (response.data.status === 200) {
          history('/pembayaran_berhasil');
        }
      })
      .catch((error) => {});
  };
};

const postUploadPembayaranSuccess = (payload) => ({
  type: 'postUploadPembayaran-success',
  payload,
});

const getOrderList = (status, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/order/list?token=${token}`, {
        status,
      })
      .then((response) => {
        dispatch(getOrderListSuccess(response.data.data));
      })
      .catch((error) => {});
  };
};

const getOrderListSuccess = (payload) => ({
  type: 'getOrderList-success',
  payload,
});

const getOrderDetail = (id, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/order/detail/${id}?token=${token}`, {
        id,
      })
      .then((response) => {
        dispatch(getOrderDetailSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const getOrderDetailSuccess = (payload) =>
  //// console.log("getOrderDetailSuccess", payload),
  ({
    type: 'getOrderDetailSuccess-success',
    payload,
  });

const getFlashSale = () => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/flashsale/now`)
      .then((response) => {
        dispatch(fetchFlashSaleSucess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const fetchFlashSaleSucess = (payload) => ({
  type: 'fetch-flash',
  payload,
});
const toggleNav = (payload) => ({
  type: 'toggle-nav',
  payload,
});

const postReceived = (id, token) => {
  //const {subTotalProduk, history, code, service, note, payment_account_id, productId, qty} = payload
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/order/${id}/received?token=${token}`, {
        id,
      })
      .then((response) => {
        // console.log('response data postReceived: ', response.data);
        dispatch(postReceivedSuccess);
        window.location.reload();
      })

      .catch((error) => {});
  };
};

const postReceivedSuccess = {
  type: 'postReceived/success',
};

const postReviewProduct = (rating, review, id, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/order/${id}/review?token=${token}`, {
        rating,
        review,
      })
      .then((response) => {
        dispatch(postReviewProductSuccess);
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const refreshReview = {
  type: 'refresh-review',
};

const postReviewProductSuccess = {
  type: 'review-success',
};

const postBatalkanPesanan = (id, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/order/${id}/cancel?token=${token}`, {
        id,
      })
      .then((response) => {
        dispatch(postBatalkanSuccess);
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const postBatalkanSuccess = {
  type: 'postBatalkan/success',
};

const getProductReview = (id) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/product/${id}/review`)
      .then((response) => {
        dispatch(getProductReviewSuccess(response.data.data));
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const getProductReviewSuccess = (payload) => ({
  type: 'getProduct-success',
  payload,
});

const getProfile = (token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/user/getProfileByEmail?token=${token}`)
      .then((response) => {
        // // console.log("getProfile", response.data.data)
        dispatch(getProfileSuccess(response.data.data));
      })
      .catch((error) => {
        //console.log('error', error);
      });
  };
};

const getProfileSuccess = (payload) => ({
  type: 'getProfile-success',
  payload,
});

const updateProfile = (birthdate, email, gender, name, nomor, token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .post(`${baseUrl}/api/v1/user/updateProfile?token=${token}`, {
        birthdate: birthdate,
        email: email,
        gender: gender,
        nama: name,
        nomorHp: nomor,
      })
      .then((response) => {
        window.location.reload();
      })
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

const updateProfileSuccess = {
  type: 'updateProfile/success',
};

const logOut = (token) => {
  return (dispatch, getState, baseUrl) => {
    axios
      .get(`${baseUrl}/api/v1/auth/logout?token=${token}`)
      .then((response) => {})
      .catch((error) => {
        // console.log('error', error);
      });
  };
};

export {
  toggleNav,
  getFlashSale,
  deleteCart,
  incrementCart,
  decrementCart,
  removeFromCart,
  addToCart,
  incrementItem,
  decrementItem,
  fetchRefreshState,
  loginActionAsync,
  registAccountAsync,
  checkEmailAsync,
  otpVerifAsync,
  checkEmailForgotPass,
  otpVerifForgotPass,
  createNewPass,
  // products //
  getAllProduct,
  searchByBrand,
  searchByBrandTes,
  getBrandImage,
  searchByCategory,
  searchByCategoryTes,
  getFamiliar,
  // getProductByDiscount,
  // dummyFetchApi,
  searchProductApi,
  getFilter,
  refreshFilter,
  getProductById,
  addProductToCart,
  // address action //
  getAllCities,
  getAllProvinces,
  getAllCitiesByProvince,
  saveAddress,
  getMyAddress,
  getShippingCost,
  getShippingCostButton,
  // pembayaran action //
  getAllAccount,
  onCheckOut,
  getPaymentData,
  postUploadPembayaran,
  // riwayat action //
  getOrderList,
  getOrderDetail,
  postReceived,
  postBatalkanPesanan,
  postReviewProduct,
  refreshReview,
  fetchRefreshStatePesanan,
  // get review product //
  getProductReview,
  // user //
  getProfile,
  updateProfile,
  logOut,
};
