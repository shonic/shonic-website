const initialState = {
  list: [],
  listSearch: [],
  listBrandProducts: [],
  listCategoryProducts: [],
  brandImage: '',
  searchFilter: true,
  listId: {},
  fullName: '',
  keyword: '',
  brand: 'a',
  category: '',
  rekomendasiState: '5',
  error: '',
  loading: false,
  produkId: '',
  DummyName: '',
  DummyPrice: '',
  cart: [],
  cartSubtotal: 0,
  cartTotal: 0,
  account: [],
  listFlash: [],
  startFlashSale: '',
  endFlashSale: '',
  listReview: {},
  listFamiliar: [],
};

export default function homepage_reducer(state = initialState, action) {
  const { type, payload, keyword, brand_name, category_name, ID, detailTotal, detailQuantity, hargaBarang } = action;
  switch (type) {
    case 'fetch-product-success':
      return {
        ...state,
        list: payload,
        loading: false,
      };
    case 'fetch-product-failed':
      return {
        ...state,
        error: payload,
      };
    case 'fetch-familiar-success':
      return {
        ...state,
        listFamiliar: payload,
      };
    case 'search-product-success':
      return {
        ...state,
        listSearch: payload,
        keyword: keyword,
        searchFilter: true,
      };
    case 'search-filter-failed':
      return {
        ...state,
        listSearch: payload,
        searchFilter: false,
      };
    case 'refresh-filter':
      return {
        ...state,
        searchFilter: true,
      };
    case 'search-product-failed':
      return {
        ...state,
        listSearch: payload,
      };
    case 'search-brand-success':
      return {
        ...state,
        listBrandProducts: payload,
        brand: brand_name,
        searchFilter: true,
      };
    case 'get-brand-image':
      return {
        ...state,
        image: payload,
      };
    case 'search-category-success':
      return {
        ...state,
        listCategoryProducts: payload,
        category: category_name,
        searchFilter: true,
      };
    case 'account-success':
      return {
        ...state,
        account: payload,
      };
    case 'fetch-id-success':
      return {
        ...state,
        listId: payload,
        loading: false,
      };
    case 'fetch-id-failed':
      return {
        ...state,
        error: payload,
      };
    case 'add-to-cart':
      const x = state.listSearch.find((produk) => produk['id'] === ID);
      const x2 = state.listBrandProducts.find((produk) => produk['id'] === ID);
      const x3 = state.list.find((produk) => produk['id'] === ID);
      const x4 = state.listCategoryProducts.find((produk) => produk['id'] === ID);
      console.table('x2', x2);
      const y = state.cart.find((item) => (item.id === ID ? true : false));
      return {
        ...state,
        cart: y
          ? state.cart.map((item) =>
              item.id === ID
                ? {
                    ...item,
                    jumlah: detailQuantity,
                    subHarga: hargaBarang * detailQuantity,
                    subQuantity: detailQuantity,
                    inCart: true,
                  }
                : item
            )
          : [...state.cart, { ...x, ...x2, ...x3, ...x4, jumlah: detailQuantity, subHarga: hargaBarang * detailQuantity, subQuantity: detailQuantity, inCart: true }],
        cartSubtotal: state.cartSubtotal + state.cart.reduce((acc, item) => acc + item.subHarga, 0),
      };

    case 'onCheckOut-success':
      return {
        ...state,
        cartSubtotal: payload,
      };
    case 'remove-from-cart':
      return {
        ...state,
        list: state.listSearch.map((item) => {
          item.id === payload ? (item.inCart = false) : (item.inCart = false);
          item.jumlah = 1;
          return item;
        }),
      };
    case 'increment-cart':
      return {
        ...state,
        cart: state.cart.map((item) => {
          if (item.id === payload) {
            item.jumlah += 1;
            item.subHarga = item.price * item.jumlah;
            item.subQuantity = item.jumlah;
            item.inCart = true;
          }
          return item;
        }),
      };
    case 'decrement-cart':
      return {
        ...state,
        cart: state.cart.map((item) => {
          if (item.id === payload) {
            item.jumlah <= 1 ? (item.jumlah = 1) : (item.jumlah -= 1);
            item.subHarga = item.price * item.jumlah;
            item.subQuantity = item.jumlah;
          }
          return item;
        }),
      };
    case 'delete-cart':
      return {
        ...state,
        cart: state.cart.filter((item) => item.id !== payload),
      };

    case 'fetch-flash':
      return {
        ...state,
        listFlash: payload,
        loading: false,
      };
    case 'getProduct-success':
      return {
        ...state,
        listReview: payload,
      };

    default:
      return state;
  }
}
