const initialState = {
  email: '',
  password: '',
  fullName: '',
  loading: false,
  emailStatus: true,
  verifAcc: undefined,
  verifPass: true,
  tokenPass: '',
  error: '',
  status: true,
  profile: [],
};

export default function auth_reducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case 'fetch-refresh':
      return {
        ...state,
        emailStatus: true,
        verifAcc: true,
        status: true,
        verifPass: true,
      };
    case 'login/success':
      return {
        ...state,
        ...payload,
      };
    case 'check-email/success':
      return {
        ...state,
        email: payload,
        emailStatus: true,
        verifPass: true,
      };
    case 'check-email/failed':
      return {
        ...state,
        emailStatus: false,
      };
    case 'otp-verif/success':
      return {
        ...state,
        verifAcc: true,
      };
    case 'otp-verif/fail':
      return {
        ...state,
        verifAcc: false,
      };
    case 'regist/success':
      return {
        ...state,
        fullName: payload,
      };
    case 'otp-verif-forgotPass/success':
      return {
        ...state,
        verifPass: true,
        tokenPass: payload,
      };
    case 'otp-verif-forgotPass/fail':
      return {
        ...state,
        verifPass: false,
      };
    case 'create-new-pass/success':
      return {
        ...state,
        password: payload,
      };
    case 'LOG_SUCCESS':
      return {
        ...state,
        status: true,
      };
    case 'LOG_FAILED':
      return {
        ...state,
        status: false,
      };

    case 'getProfile-success':
      return {
        ...state,
        profile: payload,
      };
    default:
      return state;
  }
}
