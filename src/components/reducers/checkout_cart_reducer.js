const initialState = {
  namaPenerima: '',
  nomorHp: 0,
  provinsi: '',
  kota: '',
  kecamatan: '',
  kodePos: '',
  alamat: '',
  allProvinces: [],
  allCitiesByProvince: [],
  allCities: [],
  savedAddress: [],
  shipping: [],
  paymentData: [],
  uploadPembayaran: '',
  riwayatPemesanan: [],
  orderDetail: [],
  reviewSuccess: false,
  statusDiterima: false,
  statusDibatalkan: false,
  statusProfile: false,
};

export default function checkout_cart_reducer(state = initialState, action) {
  const { payload, type } = action;
  switch (type) {
    case 'fetch-provinces-success':
      return {
        ...state,
        allProvinces: payload,
      };
    case 'fetch-citiesByProvince-success':
      return {
        ...state,
        allCitiesByProvince: payload,
      };
    case 'fetch-allCities-success':
      return {
        ...state,
        allCities: payload,
      };
    case 'post-saveAddress-success':
      return {
        ...state,
        savedAddress: payload,
      };
    case 'shippingCost-success':
      return {
        ...state,
        shipping: payload,
      };
    case 'getPaymentData-success':
      return {
        ...state,
        paymentData: payload,
      };
    case 'postUploadPembayaran-success':
      return {
        ...state,
        uploadPembayaran: payload,
      };
    case 'getOrderList-success':
      return {
        ...state,
        riwayatPemesanan: payload,
      };
    case 'getOrderDetailSuccess-success':
      return {
        ...state,
        orderDetail: payload,
      };
    case 'review-success':
      return {
        ...state,
        reviewSuccess: true,
      };
    case 'refresh-review':
      return {
        ...state,
        reviewSuccess: false,
      };
    case 'postReceived/success':
      return {
        ...state,
        statusDiterima: true,
      };
    case 'postBatalkan/success':
      return {
        ...state,
        statusDibatalkan: true,
      };
    case 'fetchPesanan-refresh':
      return {
        ...state,
        statusDiterima: false,
        statusDibatalkan: false,
        reviewSuccess: false,
        statusProfile: false,
      };
    case 'updateProfile/success':
      return {
        ...state,
        statusProfile: true,
      };
    default:
      return state;
  }
}
