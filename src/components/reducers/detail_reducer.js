const initialState = {
  detailQuantity: 0,
  detailTotal: 0,
};

export default function detail_reducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case 'increment-item':
      return {
        ...state,
        detailQuantity: state.detailQuantity + 1,
        detailTotal: (state.detailQuantity + 1) * payload,
      };
    case 'decrement-item':
      return {
        ...state,
        detailQuantity: state.detailQuantity <= 1 ? 1 : state.detailQuantity - 1,
        detailTotal: state.detailQuantity <= 1 ? payload : payload * (state.detailQuantity - 1),
      };
    default:
      return state;
  }
}
