import { combineReducers } from 'redux';
import auth_reducer from './auth_reducer';
import detail_reducer from './detail_reducer';
import homepage_reducer from './homepage_reducer';
import nav_reducer from './nav_reducer';
import checkout_cart_reducer from './checkout_cart_reducer';

const reducer = combineReducers({
  auth_reducer: auth_reducer,
  homepage_reducer: homepage_reducer,
  detail_reducer: detail_reducer,
  checkout_cart_reducer: checkout_cart_reducer,
  nav_reducer: nav_reducer,
});

export default reducer;
