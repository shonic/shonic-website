const initialState = {
  filterParameter: 'relevan',
};

export default function discount_reducer(state = initialState, action) {
  const { type, filtering } = action;
  switch (type) {
    case 'filter-discount':
      return {
        ...state,
        filterParameter: filtering,
      };
    default:
      return state;
  }
}
