const initialState = {
  hamburger: false,
};

export default function detail_reducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case 'toggle-nav':
      return {
        ...state,
        hamburger: !state.hamburger,
      };
    default:
      return state;
  }
}
