import styles from './LoginNext.module.scss';
import { Link, useNavigate } from 'react-router-dom';
import { EyeClosed, EyeOpen, LeftButton } from '../../images/icons/ShonicIcon';
import { useDispatch, useSelector } from 'react-redux';
import { registAccountAsync } from '../action';
import { useState } from 'react';

const LoginNext = () => {
  const [fullName, setFullName] = useState('');
  const [password, setPassword] = useState('');
  const [konfirmasiPassword, setKonfirmasiPassword] = useState('');
  const [errorPass, setErrorPass] = useState('');

  const [passwordType, setPasswordType] = useState('password');
  const [passwordTypeConfirm, setPasswordTypeConfirm] = useState('password');
  const { email } = useSelector((state) => state.auth_reducer);

  let history = useNavigate();
  const dispatch = useDispatch();

  const togglePasswordType = (e) => {
    e.preventDefault();
    if (passwordType === 'password') {
      setPasswordType('text');
    } else {
      setPasswordType('password');
    }
  };
  const togglePasswordTypeConfirm = (e) => {
    e.preventDefault();
    if (passwordTypeConfirm === 'password') {
      setPasswordTypeConfirm('text');
    } else {
      setPasswordTypeConfirm('password');
    }
  };

  const createAccount = (e) => {
    e.preventDefault();
    if (validation(e)) {
      dispatch(registAccountAsync(email, fullName, password, history));
    }
  };

  const validation = (e) => {
    let isValid = false;
    let konfirmasiInput = e.target.parentNode.childNodes[2].childNodes[1];
    if (password !== konfirmasiPassword) {
      setErrorPass('Password tidak sama');
      konfirmasiInput.style.cssText = 'border:1px solid #CB3A31; border-radius:6px;';
      isValid = false;
    } else {
      setErrorPass('');
      isValid = true;
    }
    return isValid;
  };

  return (
    <div className={styles.outer}>
      <div className={`${styles.flexcontainer} container`}>
        <div className={styles.flexbottom}>
          <div className={styles.head}>
            <Link to="/verifikasi">
              <LeftButton className={styles.left} />
            </Link>
            <h3 className={`${styles.h3} bold-16-res`}>Lengkapi Pendaftaran</h3>
          </div>
          <p className={`${styles.syarat} medium-12`}>
            Kode berhasil dikirim melalui email <span>{email}</span>, periksa dan masukkan kode disini untuk dapat membuat akun baru
          </p>

          <form className={styles.form}>
            <div className={styles.div}>
              <label className={`${styles.label} medium-14`}>Nama Lengkap</label>
              <input className={`${styles.input} regular-14`} type="email" placeholder="Contoh: Rachel Anastasya" name="name" value={fullName} onChange={(e) => setFullName(e.target.value)} />
            </div>
            <div className={styles.div}>
              <label className={`${styles.label} medium-14`}>password</label>
              <div className={styles.flexinput}>
                <input className={`${styles.input} regular-14`} type={passwordType} placeholder="masukkan password" name="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                <button className={styles.password} onClick={togglePasswordType}>
                  {passwordType === 'password' ? <EyeClosed /> : <EyeOpen />}
                </button>
              </div>
            </div>
            <div className={styles.div}>
              <label className={`${styles.label} medium-14`}>Konfirmasi password</label>
              <div className={styles.flexinput}>
                <input className={`${styles.input} regular-14`} type={passwordTypeConfirm} placeholder="masukkan password" name="password" value={konfirmasiPassword} onChange={(e) => setKonfirmasiPassword(e.target.value)} />
                <button className={styles.password} onClick={togglePasswordTypeConfirm}>
                  {passwordTypeConfirm === 'password' ? <EyeClosed /> : <EyeOpen />}
                </button>
              </div>
              <span className={`${styles.span} regular-12`}>{errorPass}</span>
            </div>
            <button className={`${styles.button} semibold-16`} onClick={createAccount}>
              Daftar
            </button>
          </form>
          <p className={`${styles.daftar} medium-14`}>
            Tidak menerima kode verifikasi?
            <span> </span>
            <Link to="/" className={styles.link}>
              Kirim Ulang
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default LoginNext;
