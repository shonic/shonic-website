import { LoginPic } from '../../images/pictures/LoginPic';
import styles from './Register.module.scss';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { checkEmailAsync, fetchRefreshState } from '../action';
import { useState, useEffect } from 'react';
import PopUp from '../auth_component/PopUp';

const Register = () => {
  const [email, setEmail] = useState('');
  const [errorEmail, setErrorEmail] = useState('');
  const { emailStatus } = useSelector((state) => state.auth_reducer);
  const [open, setOpen] = useState(emailStatus);
  const [open2, setOpen2] = useState(true);

  const history = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchRefreshState);
  }, []);

  const onRefreshState = () => {
    dispatch(fetchRefreshState);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (validation(e)) {
      dispatch(checkEmailAsync(email, history));
      setErrorEmail('');
    }
  };

  const validation = (e) => {
    let isValid = true;
    let emailInput = e.target.parentNode.childNodes[0].childNodes[1];
    const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (email && regex.test(email) === true) {
      setErrorEmail('');
      emailInput.style.cssText = 'border:1px solid #e0e0e0';
      isValid = true;
    } else if (!email || regex.test(email) === false) {
      setErrorEmail('Format email tidak valid, contoh: shonic@gmail.com');
      emailInput.style.cssText = 'border:1px solid red';
      isValid = false;
    }
    return isValid;
  };

  return (
    <div className={styles.outer}>
      <div className={`${styles.flexcontainer} container`}>
        <div className={styles.flexup}>
          <LoginPic className={styles.pic} />
        </div>
        <div className={styles.flexbottom}>
          <h3 className={`${styles.h3} bold-24`}>daftar</h3>
          <form className={styles.form}>
            <div className={styles.div}>
              <label className={`${styles.label} medium-14`}>email</label>
              <input className={`${styles.input} regular-14`} type="email" placeholder="masukkan email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
              <span className={`${styles.span} regular-12`}>{errorEmail}</span>
            </div>
            <p className={`${styles.syarat} medium-12`}>
              Dengan mendaftar, saya menyetujui
              <span> Syarat dan Ketentuan</span> serta
              <span> Kebijakan Privasi</span>
            </p>
            <button className={`${styles.button} semibold-16`} onClick={onSubmit}>
              Lanjut
            </button>
          </form>
          {emailStatus !== true ? (
            <>
              <PopUp open={open2} email={email} onClose={onRefreshState} />
            </>
          ) : null}
          <p className={`${styles.daftar} medium-14`}>
            Sudah punya akun?
            <span> </span>
            <Link to="/login" className={styles.link}>
              Login
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Register;
