import styles from './ResetPassNext.module.scss';
import { EyeClosed, EyeOpen, LeftButton } from '../../images/icons/ShonicIcon';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createNewPass, fetchRefreshState } from '../action';

const ResetPassNext = () => {
  const [newPass, setNewPass] = useState('');
  const [newPassCheck, setNewPassCheck] = useState('');
  const [errorPass, setErrorPass] = useState('');
  const { tokenPass, email, verifPass } = useSelector((state) => state.auth_reducer);
  const [passwordType, setPasswordType] = useState('password');
  const [passwordTypeConfirm, setPasswordTypeConfirm] = useState('password');

  let history = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchRefreshState);
  }, []);

  const resetPass = (e) => {
    e.preventDefault();
    if (validation(e)) {
      dispatch(createNewPass(email, newPass, tokenPass, history));
    }
  };

  const validation = (e) => {
    let isValid = false;
    let konfirmasiInput = e.target.parentNode.childNodes[1].childNodes[1];
    if (newPass !== newPassCheck) {
      setErrorPass('Password tidak sama');
      konfirmasiInput.style.cssText = 'border:1px solid #CB3A31; border-radius: 6px;';
      isValid = false;
    } else {
      setErrorPass('');
      isValid = true;
    }
    return isValid;
  };

  const togglePasswordType = (e) => {
    e.preventDefault();
    if (passwordType === 'password') {
      setPasswordType('text');
    } else {
      setPasswordType('password');
    }
  };

  const togglePasswordTypeConfirm = (e) => {
    e.preventDefault();
    if (passwordTypeConfirm === 'password') {
      setPasswordTypeConfirm('text');
    } else {
      setPasswordTypeConfirm('password');
    }
  };

  return (
    <div className={styles.outer}>
      <div className={`${styles.flexcontainer} container`}>
        <div className={styles.flexbottom}>
          <div className={styles.head}>
            <Link to="/resetpass_verif">
              <LeftButton className={styles.left} />
            </Link>
            <h3 className={`${styles.h3} semibold-25`}>Atur Ulang Password </h3>
          </div>
          {verifPass === false ? (
            <>
              <div className={`${styles.danger} medium-12`}>
                <p>Password baru harus berbeda dengan password lama</p>
              </div>
            </>
          ) : null}
          <form className={styles.form}>
            <div className={styles.div}>
              <label className={`${styles.label} medium-14`}>Password Baru</label>
              <div className={styles.flexinput}>
                <input className={`${styles.input} regular-14`} type={passwordType} placeholder="password" name="password" value={newPass} onChange={(e) => setNewPass(e.target.value)} />
                <button className={styles.password} onClick={togglePasswordType}>
                  {passwordType === 'password' ? <EyeClosed /> : <EyeOpen />}
                </button>
              </div>
            </div>
            <div className={styles.div}>
              <label className={`${styles.label} medium-14`}>Konfirmasi Password Baru</label>
              <div className={styles.flexinput}>
                <input className={`${styles.input} regular-14`} type={passwordTypeConfirm} placeholder="password" name="password" value={newPassCheck} onChange={(e) => setNewPassCheck(e.target.value)} />
                <button className={styles.password} onClick={togglePasswordTypeConfirm}>
                  {passwordTypeConfirm === 'password' ? <EyeClosed /> : <EyeOpen />}
                </button>
              </div>
              <span className={`${styles.span} regular-12`}>{errorPass}</span>
            </div>

            <button className={`${styles.button} semibold-16`} onClick={resetPass}>
              Reset Password
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ResetPassNext;
