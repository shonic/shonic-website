import styles from './ResetPassVerif.module.scss';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { LeftButton } from '../../images/icons/ShonicIcon';
import { fetchRefreshState, otpVerifForgotPass, checkEmailForgotPass } from '../action';

const ResetPassVerif = () => {
  const { email, verifPass } = useSelector((state) => state.auth_reducer);
  const inputElements = [...document.querySelectorAll('input')];

  const dispatch = useDispatch();
  let history = useNavigate();

  useEffect(() => {
    dispatch(fetchRefreshState);
  }, []);

  const onVerification = (e) => {
    e.preventDefault();
    const code = [...document.getElementsByTagName('input')]
      .filter(({ name }) => name)
      .map(({ value }) => value)
      .join('');
    dispatch(otpVerifForgotPass(email, code, history));
  };

  const onResendCode = (e) => {
    dispatch(checkEmailForgotPass(email, history));
  };

  inputElements.forEach((ele, index) => {
    ele.addEventListener('keydown', (e) => {
      if (e.keyCode === 8 && e.target.value === '') inputElements[Math.max(0, index - 1)].focus();
    });
    ele.addEventListener('input', (e) => {
      const [first, ...rest] = e.target.value;
      e.target.value = first ?? '';
      const lastInputBox = index === inputElements.length - 1;
      const insertedContent = first !== undefined;
      if (insertedContent) {
        inputElements[index + 1].focus();
      }
    });
  });

  return (
    <div className={styles.outer}>
      <div className={`${styles.flexcontainer} container`}>
        <div className={styles.flexbottom}>
          <div className={styles.head}>
            <Link to="/resetpass">
              <LeftButton className={styles.left} />
            </Link>
            <h3 className={`${styles.h3} bold-16-res`}>Verifikasi Password</h3>
          </div>
          {verifPass === false ? (
            <>
              <div className={`${styles.danger} medium-12`}>
                <p> Kode yang Anda masukkan salah</p>
              </div>
            </>
          ) : null}
          <p className={`${styles.syarat} medium-12`}>
            Kode berhasil dikirim melalui email <span>{email}</span>, periksa dan masukkan kode disini untuk dapat melakukan reset password
          </p>

          <form className={styles.form}>
            <div className={styles.flexform}>
              <input className={`${styles.input} regular-14 tes`} type="text" name="text" required />
              <input className={`${styles.input} regular-14`} type="text" name="text" required />
              <input className={`${styles.input} regular-14`} type="text" name="text" required />
              <input className={`${styles.input} regular-14`} type="text" name="text" required />
              <input className={`${styles.input} regular-14`} type="text" name="text" required />
              <input className={`${styles.input} regular-14`} type="text" name="text" required />
            </div>
            <button className={`${styles.button} semibold-16`} onClick={onVerification}>
              Verifikasi
            </button>
          </form>
          <p className={`${styles.daftar} medium-14`}>
            Tidak menerima kode verifikasi? <span onClick={onResendCode}> Kirim Ulang</span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default ResetPassVerif;
